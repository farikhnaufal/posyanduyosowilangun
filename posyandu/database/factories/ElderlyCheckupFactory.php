<?php
namespace Database\Factories;

use App\Models\Elderly;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\ElderlyCheckup;

class ElderlyCheckupFactory extends Factory
{
    protected $model = ElderlyCheckup::class;

    public function definition()
    {
        $faker = app(Faker::class);

        // Get an existing 'Elderly' ID from the database
        $elderlyIds = Elderly::pluck('id')->all();
        $elderlyId = $faker->randomElement($elderlyIds);

        return [
            'weight' => $faker->numberBetween(40, 120),
            'height' => $faker->numberBetween(140, 200),
            'stomach' => $faker->numberBetween(60, 150),
            'tensionA' => $faker->numberBetween(90, 160),
            'tensionB' => $faker->numberBetween(60, 90),
            'cholesterol' => $faker->numberBetween(100, 300),
            'sugar' => $faker->numberBetween(60, 150),
            'gout' => $faker->randomFloat(2, 2, 8),
            'independence' => $faker->randomElement(['Mandiri', 'Sebagian', 'Total']),
            'complaint' => $faker->sentence(),
            'diagnosis' => $faker->sentence(),
            'followUp' => $faker->randomElement(['Rujukan', 'Konseling']),
            'therapy' => $faker->sentence(),
            'checkupDate' => $faker->dateTimeBetween('-1 year', 'now'),
            'elderly_id' => $elderlyId,
        ];
    }
}


