<?php

namespace Database\Factories;

use App\Models\Elderly;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Elderly>
 */
class ElderlyFactory extends Factory
{

    protected $model = Elderly::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $start = Carbon::create('1953', '01', '01');
        $end = Carbon::create('1983', '01', '01');
        return [
            //
            'name' => $this->faker->name,
            'gender' => $this->faker->randomElement(['Laki-Laki', 'Perempuan']),
            'address' => $this->faker->address,
            'bornPlace' => $this->faker->city,
            'bornDate' => $this->faker->dateTimeBetween($start, $end),
            'nik' => $this->faker->unique()->randomNumber(8, true),
            'ktp' => $this->faker->randomElement(['KTP Yosowilangun', 'Non KTP Yosowilangun']),
            'disease' => $this->faker->randomElement(['diabetes', 'hypertension', 'asthma']),
            'family' => $this->faker->name,
            'status' => $this->faker->randomElement(['Hidup']),
            'familyPhone' => $this->faker->phoneNumber,
            'pos_id' => $this->faker->randomElement(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16'])
        ];
    }
}
