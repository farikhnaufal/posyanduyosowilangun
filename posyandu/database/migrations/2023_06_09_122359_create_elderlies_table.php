<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('elderlies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('gender', ['Laki-Laki', 'Perempuan']);
            $table->string('address');
            $table->string('bornPlace');
            $table->date('bornDate');
            $table->string('nik');
            $table->enum('ktp', ['KTP Yosowilangun', 'Non KTP Yosowilangun']);
            $table->string('disease')->nullable();
            $table->enum('status', ['Hidup', 'Meninggal']);
            $table->string('family')->nullable();
            $table->string('familyPhone')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('pos_id');

            $table->foreign('pos_id')->references('id')->on('pos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('elderlies');
    }
};
