<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('elderly_checkups', function (Blueprint $table) {
            $table->id();
            $table->integer('weight');
            $table->integer('height');
            $table->integer('stomach');
            $table->integer('tensionA');
            $table->integer('tensionB');
            $table->integer('cholesterol');
            $table->integer('sugar');
            $table->float('gout');
            $table->enum('independence', ['Mandiri', 'Sebagian', 'Total']);
            $table->longText('complaint')->nullable();
            $table->longText('diagnosis')->nullable();
            $table->enum('followUp', ['Rujukan', 'Konseling']);
            $table->string('therapy');
            $table->date('checkupDate');
            $table->timestamps();
            $table->unsignedBigInteger('elderly_id');

            $table->foreign('elderly_id')->references('id')->on('elderlies')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('elderly_checkups');
    }
};
