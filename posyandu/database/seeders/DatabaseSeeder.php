<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Baby;
use App\Models\Elderly;
use App\Models\ElderlyCheckup;
use App\Models\Pos;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        Permission::create(['name' => 'create-lansia']);
        Permission::create(['name' => 'edit-lansia']);
        Permission::create(['name' => 'delete-lansia']);
        Permission::create(['name' => 'create-user']);
        Permission::create(['name' => 'edit-user']);
        Permission::create(['name' => 'delete-user']);
        Permission::create(['name' => 'create-checkup']);
        Permission::create(['name' => 'edit-checkup']);
        Permission::create(['name' => 'delete-checkup']);

        $adminRole = Role::create(['name' => 'admin']);
        $kaderRole = Role::create(['name' => 'kader']);
        $koordinatorRole = Role::create(['name' => 'koordinator']);

        $adminRole->givePermissionTo([
            'create-lansia',
            'edit-lansia',
            'delete-lansia',
            'create-user',
            'edit-user',
            'delete-user',
            'create-checkup',
            'edit-checkup',
            'delete-checkup',

        ]);

        $koordinatorRole->givePermissionTo([
            'edit-checkup',
        ]);

        $kaderRole->givePermissionTo([
            'create-lansia',
            'edit-lansia',
            'delete-lansia',
            'create-checkup',
            'edit-checkup',
            'delete-checkup',
        ]);

        Pos::create([
            'name' => 'Pos 1'
        ]);
        Pos::create([
            'name' => 'Pos 2'
        ]);

        Pos::create([
            'name' => 'Pos 3'
        ]);
        Pos::create([
            'name' => 'Pos 4'
        ]);
        Pos::create([
            'name' => 'Pos 5'
        ]);
        Pos::create([
            'name' => 'Pos 6'
        ]);
        Pos::create([
            'name' => 'Pos 7'
        ]);
        Pos::create([
            'name' => 'Pos 8'
        ]);
        Pos::create([
            'name' => 'Pos 9'
        ]);
        Pos::create([
            'name' => 'Pos 10'
        ]);
        Pos::create([
            'name' => 'Pos 11'
        ]);
        Pos::create([
            'name' => 'Pos 12'
        ]);
        Pos::create([
            'name' => 'Pos 13'
        ]);
        Pos::create([
            'name' => 'Pos 14'
        ]);
        Pos::create([
            'name' => 'Pos 15'
        ]);
        Pos::create([
            'name' => 'Pos 16'
        ]);


        User::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'image' => '',

        ])->assignRole('admin');


        User::create([
            'name' => 'Kader Pos 1',
            'username' => 'kader1',
            'password' => bcrypt('kader1'),
            'image' => '',
            'pos_id' => 1,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 2',
            'username' => 'kader2',
            'password' => bcrypt('kader2'),
            'image' => '',
            'pos_id' => 2,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 3',
            'username' => 'kader3',
            'password' => bcrypt('kader3'),
            'image' => '',
            'pos_id' => 3,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 4',
            'username' => 'kader4',
            'password' => bcrypt('kader4'),
            'image' => '',
            'pos_id' => 4,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 5',
            'username' => 'kader5',
            'password' => bcrypt('kader5'),
            'image' => '',
            'pos_id' => 5,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 6',
            'username' => 'kader6',
            'password' => bcrypt('kader6'),
            'image' => '',
            'pos_id' => 6,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 7',
            'username' => 'kader7',
            'password' => bcrypt('kader7'),
            'image' => '',
            'pos_id' => 7,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 8',
            'username' => 'kader8',
            'password' => bcrypt('kader8'),
            'image' => '',
            'pos_id' => 8,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 9',
            'username' => 'kader9',
            'password' => bcrypt('kader9'),
            'image' => '',
            'pos_id' => 9,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 10',
            'username' => 'kader10',
            'password' => bcrypt('kader10'),
            'image' => '',
            'pos_id' => 10,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 11',
            'username' => 'kader11',
            'password' => bcrypt('kader11'),
            'image' => '',
            'pos_id' => 11,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 12',
            'username' => 'kader12',
            'password' => bcrypt('kader12'),
            'image' => '',
            'pos_id' => 12,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 13',
            'username' => 'kader13',
            'password' => bcrypt('kader13'),
            'image' => '',
            'pos_id' => 13,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 14',
            'username' => 'kader14',
            'password' => bcrypt('kader14'),
            'image' => '',
            'pos_id' => 14,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 15',
            'username' => 'kader15',
            'password' => bcrypt('kader15'),
            'image' => '',
            'pos_id' => 15,
        ])->assignRole('kader');
        User::create([
            'name' => 'Kader Pos 16',
            'username' => 'kader16',
            'password' => bcrypt('kader16'),
            'image' => '',
            'pos_id' => 16,
        ])->assignRole('kader');


        User::create([
            'name' => 'Koordinator Pos 1',
            'username' => 'koordinator1',
            'password' => bcrypt('koordinator1'),
            'image' => '',
            'pos_id' => 1,
        ])->assignRole('koordinator');

        User::create([
            'name' => 'Koordinator Pos 2',
            'username' => 'koordinator2',
            'password' => bcrypt('koordinator2'),
            'image' => '',
            'pos_id' => 2,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 3',
            'username' => 'koordinator3',
            'password' => bcrypt('koordinator3'),
            'image' => '',
            'pos_id' => 3,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 4',
            'username' => 'koordinator4',
            'password' => bcrypt('koordinator4'),
            'image' => '',
            'pos_id' => 4,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 5',
            'username' => 'koordinator5',
            'password' => bcrypt('koordinator5'),
            'image' => '',
            'pos_id' => 5,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 6',
            'username' => 'koordinator6',
            'password' => bcrypt('koordinator6'),
            'image' => '',
            'pos_id' => 6,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 7',
            'username' => 'koordinator7',
            'password' => bcrypt('koordinator7'),
            'image' => '',
            'pos_id' => 7,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 8',
            'username' => 'koordinator8',
            'password' => bcrypt('koordinator8'),
            'image' => '',
            'pos_id' => 8,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 9',
            'username' => 'koordinator9',
            'password' => bcrypt('koordinator9'),
            'image' => '',
            'pos_id' => 9,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 10',
            'username' => 'koordinator10',
            'password' => bcrypt('koordinator10'),
            'image' => '',
            'pos_id' => 10,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 11',
            'username' => 'koordinator11',
            'password' => bcrypt('koordinator11'),
            'image' => '',
            'pos_id' => 11,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 12',
            'username' => 'koordinator12',
            'password' => bcrypt('koordinator12'),
            'image' => '',
            'pos_id' => 12,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 13',
            'username' => 'koordinator13',
            'password' => bcrypt('koordinator13'),
            'image' => '',
            'pos_id' => 13,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 14',
            'username' => 'koordinator14',
            'password' => bcrypt('koordinator14'),
            'image' => '',
            'pos_id' => 14,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 15',
            'username' => 'koordinator15',
            'password' => bcrypt('koordinator15'),
            'image' => '',
            'pos_id' => 15,
        ])->assignRole('koordinator');
        User::create([
            'name' => 'Koordinator Pos 16',
            'username' => 'koordinator16',
            'password' => bcrypt('koordinator16'),
            'image' => '',
            'pos_id' => 16,
        ])->assignRole('koordinator');

        //

        // Elderly::create([
        //     'name' => 'Dono Pradono',
        //     'gender' => 1,
        //     'address' => 'Ds. Balun, Kec. Turi, Kab. Lamongan',
        //     'bornPlace' => 'Lamongan',
        //     'bornDate' => Carbon::create('1945', '8', '17'),
        //     'nik' => '3012010020',
        //     'ktp' => 1,
        //     'disease' => 'Tidak ada',
        //     'status' => 1,
        //     'family' => 'Dona',
        //     'familyPhone' => '081358620667',
        //     'pos_id' => 1,
        // ]);
        // Elderly::create([
        //     'name' => 'Yoga Frimawan',
        //     'gender' => 1,
        //     'address' => 'Ds. Candipari, Kec. Turi, Kab. Lamongan',
        //     'bornPlace' => 'Lamongan',
        //     'bornDate' => Carbon::create('2000', '8', '17'),
        //     'nik' => '3012010021',
        //     'ktp' => 1,
        //     'disease' => 'Diabetes',
        //     'status' => 2,
        //     'family' => 'Yogi',
        //     'familyPhone' => '081358622234',
        //     'pos_id' => 1,
        // ]);



        // //
        // ElderlyCheckup::create([
        //     'weight' => 60,
        //     'height' => 175,
        //     'stomach' => 80,
        //     'tensionA' => 130,
        //     'tensionB' => 80,
        //     'cholesterol' => 20,
        //     'sugar' => 10,
        //     'gout' => 20,
        //     'independence' => 1,
        //     'complaint' => 'Ok',
        //     'diagnosis' => 'Diagnosis aman',
        //     'followUp' => 2,
        //     'therapy' => 'Terapi lorem ipsum sit dolor amet',
        //     'checkupDate' => Carbon::create('2023', '4', '8'),
        //     'quartal' => 2,
        //     'elderly_id' => 1,
        // ]);
        // ElderlyCheckup::create([
        //     'weight' => 61,
        //     'height' => 175,
        //     'stomach' => 80,
        //     'tensionA' => 120,
        //     'tensionB' => 80,
        //     'cholesterol' => 20,
        //     'sugar' => 10,
        //     'gout' => 20,
        //     'independence' => 1,
        //     'complaint' => 'Ok',
        //     'diagnosis' => 'Lorem ipsum sit dolor amet',
        //     'followUp' => 2,
        //     'therapy' => 'Terapi lorem ipsum sit dolor amet',
        //     'checkupDate' => Carbon::create('2023', '7', '11'),
        //     'quartal' => 3,
        //     'elderly_id' => 1,
        // ]);

        // ElderlyCheckup::create([
        //     'weight' => 61,
        //     'height' => 165,
        //     'stomach' => 90,
        //     'tensionA' => 130,
        //     'tensionB' => 90,
        //     'cholesterol' => 20,
        //     'sugar' => 10,
        //     'gout' => 20,
        //     'independence' => 1,
        //     'complaint' => 'Ok',
        //     'diagnosis' => 'Lorem ipsum sit dolor amet',
        //     'followUp' => 1,
        //     'therapy' => 'Terapi lorem ipsum sit dolor amet',
        //     'checkupDate' => Carbon::create('2023', '7', '11'),
        //     'quartal' => 3,
        //     'elderly_id' => 2,
        // ]);
    }
}
