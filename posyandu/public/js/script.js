var clock = document.getElementById('clock');
arrbulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

function time() {
    date = new Date();
    detik = date.getSeconds();
    menit = date.getMinutes();
    jam = date.getHours();
    hari = date.getDay();
    tanggal = date.getDate();
    bulan = date.getMonth();
    tahun = date.getFullYear();
    // document.write(tanggal + " " + arrbulan[bulan] + " " + tahun + " " + jam + " : " + menit + " : " + detik);
    clock.textContent = tanggal + " " + arrbulan[bulan] + " " + tahun + " " + jam + " : " + menit + " : " + detik;
}
setInterval(time, 1000);

// DatePicker


// Table
$(document).ready(function () {
    $('#example').DataTable({
        dom: '<"row row-cols-1 row-cols-lg-2 justify-content-between mx-0 my-3 gap-2 gap-lg-0"lf>t<"row row-cols-1 row-cols-lg-2 justify-content-between my-3 gap-2 gap-lg-0"ip><"mt-5 d-inline-block"B>',
        buttons: [
            'excelHtml5',
        ],
    });
});
$(document).ready(function () {
    $('#elderly-checkup-table').DataTable({
        dom: '<"row row-cols-1 row-cols-lg-2 justify-content-between mx-0 my-3 gap-2 gap-lg-0"lf>t<"row row-cols-1 row-cols-lg-2 justify-content-between my-3 gap-2 gap-lg-0"ip><"mt-5 d-inline-block"B>',
        buttons: [
            'excelHtml5',
        ],
        order: [
            [0, 'desc']
        ]

    });

});


$(document).ready(function () {
    var elderlyElement = document.getElementById("elderlyName");
    var elderly = elderlyElement.textContent;
    $('#elderly-checkup-detail-table').DataTable({
        dom: '<"row row-cols-1 row-cols-lg-2 justify-content-between mx-0 my-3 gap-2 gap-lg-0"lf>t<"row row-cols-1 row-cols-lg-2 justify-content-between my-3 gap-2 gap-lg-0"ip><"mt-5 d-inline-block"B>',
        order: [0, 'desc'],
        buttons: [{
            extend: 'excelHtml5',
            title: 'Data checkup ' + elderly
        }]
    });

});


// Tooltip
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
