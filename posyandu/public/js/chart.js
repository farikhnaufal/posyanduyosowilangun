// Graph umur

var ctx = document.getElementById('age-bar-chart').getContext('2d');
var umurLansia = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: elderliesAgeData.labels,
        datasets: [{
            label: 'Pra Lansia',
            data: elderliesAgeData.praLansia,
            backgroundColor: 'rgba(75, 192, 192, 0.5)'
        }, {
            label: 'Lansia',
            data: elderliesAgeData.lansia,
            backgroundColor: 'rgba(255, 99, 132, 0.5)'
        }]
    },
    options: {
        responsive: true,
        scales: {
            x: {
                beginAtZero: true,
                stacked: true
            },
            y: {
                beginAtZero: true,
                stacked: true
            }
        }
    }
});


// Graph umur