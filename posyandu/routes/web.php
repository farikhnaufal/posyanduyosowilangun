<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\ElderlyCheckupController;
use App\Http\Controllers\ElderlyController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [ChartController::class, 'showAgeChart']);
Route::get('/kependudukan', [ChartController::class, 'showKependudukanChart']);
Route::get('/quartal', [ChartController::class, 'showKuartalChart']);
Route::get('/penyakit', [ChartController::class, 'showDiseaseChart']);
Route::get('/kehadiran', [ChartController::class, 'showKehadiranChart']);
Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'login']);


Route::middleware(['auth', 'role:admin'])->group(function () {
    Route::resource('users', UserController::class);
});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('lansia', ElderlyController::class);
    Route::resource('checkup', ElderlyCheckupController::class);
    Route::post('/logout', [AuthController::class, 'logout']);
});
