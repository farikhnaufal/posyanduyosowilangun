<?php

namespace Tests\Feature;

use App\Models\Elderly;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ElderlyTest extends TestCase
{
    /**
     * A basic feature test example.
     */

    protected $admin;
    protected $lansia;
    protected $newLansia;

    public function setUp():void{
        parent::setUp();

        $this->admin = User::role('admin')->first();
        $this->lansia = Elderly::first();
        $this->newLansia = Elderly::factory(Elderly::class)->make([
            'bornDate' => now()->format('Y-m-d'),
        ])->toArray();

    }

    public function test_index_lansia_with_admin():void {
        $response = $this->actingAs($this->admin)->get('lansia');
        $response->assertStatus(200);
    }

    public function test_create_lansia_with_admin():void{
        $response = $this->actingAs($this->admin)->get('lansia/create');
        $response->assertStatus(200);
    }

    public function test_store_lansia_with_admin():void{
        $response = $this->actingAs($this->admin)->post('lansia', $this->newLansia);
        $response->assertRedirect('lansia');

        $this->assertDatabaseHas('elderlies', $this->newLansia);
    }

    public function test_show_lansia_with_admin():void {
        $response = $this->actingAs($this->admin)->get("lansia/{$this->lansia->id}");
        $response->assertStatus(200);
    }

    public function test_edit_lansia_with_admin():void{
        $response = $this->actingAs($this->admin)->get("lansia/{$this->lansia->id}/edit");
        $response->assertStatus(200);
    }

    public function test_update_lansia_with_admin():void{
        $response = $this->actingAs($this->admin)->put("lansia/{$this->lansia->id}", $this->newLansia);
        $response->assertStatus(302);

        $updatedLansia = Elderly::findOrFail($this->lansia->id)->toArray();
        foreach ($this->newLansia as $key => $value) {
            $this->assertEquals($value, $updatedLansia[$key]);
        }
    }

    public function test_delete_lansia_with_admin():void{
        $deletedLansia = Elderly::factory(Elderly::class)->create();
        $response = $this->actingAs($this->admin)->delete("lansia/{$deletedLansia->id}");

        $response->assertStatus(302);

        $this->assertDatabaseMissing('elderlies', ['id' => $deletedLansia->id]);
    }
}
