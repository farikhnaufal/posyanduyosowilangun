<?php

namespace App\Http\Controllers;

use App\Models\Elderly;
use App\Models\Pos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ElderlyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

        $user = Auth::user();

        if ($user->hasRole('admin')) {
            # code...
            $elderlies = Elderly::select('id', 'nik', 'name', 'pos_id', 'address', 'family', 'familyPhone', 'status')
            ->orderByRaw("CAST(SUBSTRING_INDEX(pos_id, ' ', -1) AS UNSIGNED)")
            ->orderByRaw("LENGTH(pos_id), pos_id")
            ->get();
        } else {
            # code...
            $elderlies = Elderly::select('id', 'nik', 'name', 'pos_id', 'address', 'family', 'familyPhone', 'status')
            ->where('pos_id', $user->pos_id)
            ->get();
        }


        return view('lansia.data.index', compact('elderlies'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //kurang iniii 18062023
        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('kader')) {
            # code...
            $pos = Pos::all();
            return view('lansia.data.create', compact('pos'));
        } else {
            abort(403, 'UNAUTHORIZED');
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('kader')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'nik' => 'required|unique:elderlies,nik',
                'gender' => 'required',
                'address' => 'required',
                'bornPlace' => 'required',
                'bornDate' => 'required',
                'ktp' => 'required',
                'pos_id' => 'required'
            ]);

            if ($validator->fails()) {
                # code...
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }

            Elderly::create([
                'name' => $request->name,
                'gender' => $request->gender,
                'address' => $request->address,
                'bornPlace' => $request->bornPlace,
                'bornDate' => $request->bornDate,
                'nik' => $request->nik,
                'ktp' => $request->ktp,
                'disease' => $request->disease,
                'status' => $request->status,
                'family' => $request->family,
                'familyPhone' => $request->familyPhone,
                'pos_id' => $request->pos_id
            ]);
            return redirect()->intended('/lansia')->with('success', 'Data lansia berhasil ditambahkan');
        } else {
            abort(403, 'UNAUTHORIZED');
        }



    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $user = Auth::user();
        $elderly = Elderly::findOrFail($id);

        if ($user->hasRole('admin') || ($user->hasRole('kader') && $user->pos_id == $elderly->pos_id) || ($user->hasRole('koordinator') && $user->pos_id == $elderly->pos_id)) {
            return view('lansia.data.show', compact('elderly'));
        } else {
            abort(403, 'Unauthorized');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = Auth::user();
        $pos = Pos::all();
        $elderly = Elderly::findOrFail($id);

        if ($user->hasRole('admin') || ($user->hasRole('kader') && $user->pos_id == $elderly->pos_id)) {
            return view('lansia.data.edit', compact('elderly', 'pos'));
        } else {
            abort(403, 'Unauthorized');
        }
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('kader')) {
            # code...
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'nik' => 'required|unique:elderlies,nik,' . $id,
                'gender' => 'required',
                'address' => 'required',
                'bornPlace' => 'required',
                'bornDate' => 'required',
                'ktp' => 'required',
                'pos_id' => 'required'
            ]);

            if ($validator->fails()) {
                # code...
                return redirect()->back()->withErrors($validator->errors());
            }

            $elderly = Elderly::findOrFail($id);

            $elderly->update([
                'name' => $request->name,
                'gender' => $request->gender,
                'address' => $request->address,
                'bornPlace' => $request->bornPlace,
                'bornDate' => $request->bornDate,
                'nik' => $request->nik,
                'ktp' => $request->ktp,
                'disease' => $request->disease,
                'status' => $request->status,
                'family' => $request->family,
                'familyPhone' => $request->familyPhone,
                'pos_id' => $request->pos_id
            ]);

            return redirect()->back()->with('success', 'Data lansia berhasil dirubah');
        } else {
            # code...
            return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //

        $user = Auth::user();
        $elderly = Elderly::findOrFail($id);

        if ($user->hasRole('admin') || ($user->hasRole('kader') && $user->pos_id == $elderly->pos_id)) {
            $elderly->delete();
            return redirect()->back()->with('success', 'Data lansia berhasil dihapus');
        } else {
            abort(403, 'Unauthorized');
        }
    }
}
