<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //
    public function index()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            # code...
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $credentials = $validator->validated();

        if (Auth::attempt($credentials)) {
            # code...
            return redirect()->intended('/lansia');
        } else {
            # code...
            return redirect()->back()->withInput()->withErrors(['message' => 'Username or password is incorrect']);
        }
    }


    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/login');
    }
}
