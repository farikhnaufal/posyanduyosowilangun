<?php

namespace App\Http\Controllers;

use App\Models\Elderly;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    //
    public function showAgeChart()
    {
        $elderliesAge = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->select(
                'pos.name',
                DB::raw('SUM(CASE WHEN YEAR(CURRENT_DATE()) - YEAR(elderlies.bornDate) >= 60 THEN 1 ELSE 0 END) as lansia_count'),
                DB::raw('SUM(CASE WHEN YEAR(CURRENT_DATE()) - YEAR(elderlies.bornDate) < 60 THEN 1 ELSE 0 END) as pra_lansia_count')
            )
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();

        $elderliesAgeData = [
            'labels' => [],
            'praLansia' => [],
            'lansia' => [],
            'presentasePraLansia' => [],
            'presentaseLansia' => [],
        ];

        $totalPraLansia = $elderliesAge->sum('pra_lansia_count');
        $totalLansia = $elderliesAge->sum('lansia_count');
        $totalSum = $totalLansia + $totalPraLansia;

        foreach ($elderliesAge as $data) {
            $elderliesAgeData['labels'][] = $data->name;
            $elderliesAgeData['praLansia'][] = $data->pra_lansia_count;
            $elderliesAgeData['lansia'][] = $data->lansia_count;
            if ($totalLansia !== 0) {
                $elderliesAgeData['presentaseLansia'][] = round(($data->lansia_count / $totalSum) * 100, 2);
            } else {
                $elderliesAgeData['presentaseLansia'][] = 0;
            }

            if ($totalPraLansia !== 0) {
                $elderliesAgeData['presentasePraLansia'][] = round(($data->pra_lansia_count / $totalSum) * 100, 2);
            } else {
                $elderliesAgeData['presentasePraLansia'][] = 0;
            }
        }

        return view('home.ageGraph', compact('elderliesAgeData', 'totalLansia', 'totalPraLansia'));
    }


    public function showKependudukanChart()
    {
        $elderliesKtp = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->select(
                'pos.name',
                DB::raw('SUM(CASE WHEN elderlies.ktp = "KTP Yosowilangun" THEN 1 ELSE 0 END) as ktp_yoso_count'),
                DB::raw('SUM(CASE WHEN elderlies.ktp = "Non KTP Yosowilangun" THEN 1 ELSE 0 END) as ktp_non_yoso_count')
            )
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();

        $elderliesKtpData = [
            'labels' => [],
            'ktpYoso' => [],
            'ktpNonYoso' => [],
            'presentaseKtpYoso' => [],
            'presentaseKtpNonYoso' => []
        ];

        $totalKtpYoso = $elderliesKtp->sum('ktp_yoso_count');
        $totalKtpNonYoso = $elderliesKtp->sum('ktp_non_yoso_count');
        $totalSum = $totalKtpYoso + $totalKtpNonYoso;

        foreach ($elderliesKtp as $data) {
            $elderliesKtpData['labels'][] = $data->name;
            $elderliesKtpData['ktpYoso'][] = $data->ktp_yoso_count;
            $elderliesKtpData['ktpNonYoso'][] = $data->ktp_non_yoso_count;
            if ($totalKtpYoso !== 0) {
                $elderliesKtpData['presentaseKtpYoso'][] = round(($data->ktp_yoso_count / $totalSum) * 100, 2);
            } else {
                $elderliesKtpData['presentaseKtpYoso'][] = 0;
            }

            if ($totalKtpNonYoso !== 0) {
                $elderliesKtpData['presentaseKtpNonYoso'][] = round(($data->ktp_non_yoso_count / $totalSum) * 100, 2);
            } else {
                $elderliesKtpData['presentaseKtpNonYoso'][] = 0;
            }
        }

        return view('home.kependudukanGraph', compact('elderliesKtpData', 'totalKtpYoso', 'totalKtpNonYoso'));
    }


    public function showKuartalChart(Request $request){
        $quartal = $request->quartal ?? Carbon::now()->quarter.'-'.Carbon::now()->year;
        $quartalParts = explode('-', $quartal);
        $quarter = $quartalParts[0];
        $year = $quartalParts[1];

        $checkup = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->join('elderly_checkups', 'elderly_checkups.elderly_id', '=', 'elderlies.id')
            ->select(
                'pos.name',
                DB::raw('SUM(CASE WHEN elderly_checkups.cholesterol > 199 THEN 1 ELSE 0 END) as cholesterol_count'),
                DB::raw('SUM(CASE WHEN elderly_checkups.sugar > 124 THEN 1 ELSE 0 END) as sugar_count'),
                DB::raw('SUM(CASE WHEN elderly_checkups.tensionA > 139 THEN 1 ELSE 0 END) as tension_count'),
                DB::raw('SUM(CASE WHEN elderlies.gender = "Laki-Laki" AND elderly_checkups.gout > 7 THEN 1 WHEN elderlies.gender = "Perempuan" AND elderly_checkups.gout > 5 THEN 1 ELSE 0 END) as gout_count')
            )
            ->whereRaw('QUARTER(elderly_checkups.checkupDate) = ?', [$quarter])
            ->whereRaw('YEAR(elderly_checkups.checkupDate) = ?', [$year])
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();


        $checkupData = [
            'labels' => [],
            'gout' => [],
            'cholesterol' => [],
            'sugar' => [],
            'tension' => [],
            'presentaseGout' => [],
            'presentaseCholesterol' => [],
            'presentaseSugar' => [],
            'presentaseTension' => [],
        ];

        $totalGout = $checkup->sum('gout_count');
        $totalCholesterol = $checkup->sum('cholesterol_count');
        $totalSugar = $checkup->sum('sugar_count');
        $totalTension = $checkup->sum('tension_count');
        $totalSum = $totalGout + $totalCholesterol + $totalSugar + $totalTension;

        foreach ($checkup as $data) {
            $checkupData['labels'][] = $data->name;
            $checkupData['gout'][] = $data->gout_count;
            $checkupData['cholesterol'][] = $data->cholesterol_count;
            $checkupData['sugar'][] = $data->sugar_count;
            $checkupData['tension'][] = $data->tension_count;
            if ($totalGout > 0) {
                $checkupData['presentaseGout'][] = round(($data->gout_count / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseGout'][] = 0;
            }
            if ($totalCholesterol > 0) {
                $checkupData['presentaseCholesterol'][] = round(($data->cholesterol_count / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseCholesterol'][] = 0;
            }
            if ($totalSugar > 0) {
                $checkupData['presentaseSugar'][] = round(($data->sugar_count / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseSugar'][] = 0;
            }
            if ($totalTension > 0) {
                $checkupData['presentaseTension'][] = round(($data->tension_count / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseTension'][] = 0;
            }

        }

        $quartalOptions = $this->quartalOption();

         return view('home.quartalGraph', compact('checkupData', 'totalGout', 'totalCholesterol', 'totalSugar', 'totalTension', 'quartalOptions', 'quartal'));
    }


    public function showDiseaseChart(Request $request){
        $disease = $request->disease ?? 'sugar';
        $quartals = $this->getLastFourQuarters();

        if ($disease == 'cholesterol') {
            # code...
            $checkup = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->join('elderly_checkups', 'elderly_checkups.elderly_id', '=', 'elderlies.id')
            ->select(
                'pos.name',
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[0]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[0]['year'] . ' AND elderly_checkups.cholesterol > 199 THEN 1 ELSE 0 END) as qA'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[1]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[1]['year'] . ' AND elderly_checkups.cholesterol > 199 THEN 1 ELSE 0 END) as qB'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[2]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[2]['year'] . ' AND elderly_checkups.cholesterol > 199 THEN 1 ELSE 0 END) as qC'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[3]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[3]['year'] . ' AND elderly_checkups.cholesterol > 199 THEN 1 ELSE 0 END) as qD'),

            )
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();
        } elseif ($disease == 'sugar') {
            # code...
            $checkup = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->join('elderly_checkups', 'elderly_checkups.elderly_id', '=', 'elderlies.id')
            ->select(
                'pos.name',
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[0]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[0]['year'] . ' AND elderly_checkups.sugar > 124 THEN 1 ELSE 0 END) as qA'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[1]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[1]['year'] . ' AND elderly_checkups.sugar > 124 THEN 1 ELSE 0 END) as qB'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[2]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[2]['year'] . ' AND elderly_checkups.sugar > 124 THEN 1 ELSE 0 END) as qC'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[3]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[3]['year'] . ' AND elderly_checkups.sugar > 124 THEN 1 ELSE 0 END) as qD'),

            )
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();
        } elseif ($disease == 'tensionA') {
            $checkup = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->join('elderly_checkups', 'elderly_checkups.elderly_id', '=', 'elderlies.id')
            ->select(
                'pos.name',
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[0]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[0]['year'] . ' AND elderly_checkups.tensionA > 139 THEN 1 ELSE 0 END) as qA'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[1]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[1]['year'] . ' AND elderly_checkups.tensionA > 139 THEN 1 ELSE 0 END) as qB'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[2]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[2]['year'] . ' AND elderly_checkups.tensionA > 139 THEN 1 ELSE 0 END) as qC'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[3]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[3]['year'] . ' AND elderly_checkups.tensionA > 139 THEN 1 ELSE 0 END) as qD'),

            )
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();
        } elseif ($disease == 'gout') {
            $checkup = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->join('elderly_checkups', 'elderly_checkups.elderly_id', '=', 'elderlies.id')
            ->select(
                'pos.name',
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[0]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[0]['year'] . ' AND elderlies.gender = "Laki-Laki" AND elderly_checkups.gout > 7 THEN 1 WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[0]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[0]['year'] . ' AND elderlies.gender = "Perempuan" AND elderly_checkups.gout > 5 THEN 1 ELSE 0 END) as qA'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[1]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[1]['year'] . ' AND elderlies.gender = "Laki-Laki" AND elderly_checkups.gout > 7 THEN 1 WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[1]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[1]['year'] . ' AND elderlies.gender = "Perempuan" AND elderly_checkups.gout > 5 THEN 1 ELSE 0 END) as qB'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[2]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[2]['year'] . ' AND elderlies.gender = "Laki-Laki" AND elderly_checkups.gout > 7 THEN 1 WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[2]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[2]['year'] . ' AND elderlies.gender = "Perempuan" AND elderly_checkups.gout > 5 THEN 1 ELSE 0 END) as qC'),
                DB::raw('SUM(CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[3]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[3]['year'] . ' AND elderlies.gender = "Laki-Laki" AND elderly_checkups.gout > 7 THEN 1 WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[3]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[3]['year'] . ' AND elderlies.gender = "Perempuan" AND elderly_checkups.gout > 5 THEN 1 ELSE 0 END) as qD'),

            )
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();
        } else {
            return redirect()->back();
        }


        $checkupData = [
            'labels' => [],
            'qA' => [],
            'qB' => [],
            'qC' => [],
            'qD' => [],
            'presentaseqA' => [],
            'presentaseqB' => [],
            'presentaseqC' => [],
            'presentaseqD' => [],
        ];

        $totalQa = $checkup->sum('qA');
        $totalQb = $checkup->sum('qB');
        $totalQc = $checkup->sum('qC');
        $totalQd = $checkup->sum('qD');
        $totalSum = $totalQa + $totalQb + $totalQc + $totalQd;

        foreach ($checkup as $data) {
            $checkupData['labels'][] = $data->name;
            $checkupData['qA'][] = $data->qA;
            $checkupData['qB'][] = $data->qB;
            $checkupData['qC'][] = $data->qC;
            $checkupData['qD'][] = $data->qD;

            if ($totalQa > 0) {
                $checkupData['presentaseqA'][] = round(($data->qA / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseqA'][] = 0;
            }
            if ($totalQb > 0) {
                $checkupData['presentaseqB'][] = round(($data->qB / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseqB'][] = 0;
            }
            if ($totalQc > 0) {
                $checkupData['presentaseqC'][] = round(($data->qC / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseqC'][] = 0;
            }
            if ($totalQd > 0) {
                $checkupData['presentaseqD'][] = round(($data->qD / $totalSum) * 100, 2);
            } else {
                $checkupData['presentaseqD'][] = 0;
            }

        }

        $diseaseAlias = '';
        if ($disease == 'sugar') {
            $diseaseAlias = 'Gula Darah';
        } elseif ($disease == 'gout') {
            $diseaseAlias = 'Asam Urat';
        } elseif ($disease == 'tensionA') {
            $diseaseAlias = 'Darah Tinggi';
        } elseif ($disease == 'cholesterol') {
            $diseaseAlias = 'Kolesterol';
        } else {
            $diseaseAlias = '';
        }


        $diseaseOptions = $this->getPenyakitOptions();
        return view('home.diseaseGraph', compact('checkupData', 'totalQa', 'totalQb', 'totalQc', 'totalQd', 'diseaseOptions', 'disease', 'diseaseAlias','quartals'));
    }


    public function showKehadiranChart(Request $request){
        $quartals = $this->getLastFourQuarters();


        $checkup = DB::table('elderlies')
            ->join('pos', 'elderlies.pos_id', '=', 'pos.id')
            ->leftJoin('elderly_checkups', 'elderly_checkups.elderly_id', '=', 'elderlies.id')
            ->select(
                'pos.name',
                DB::raw('COUNT(DISTINCT elderlies.id) as total_elderly'),
                DB::raw('COUNT(DISTINCT CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[0]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[0]['year'] . ' THEN elderly_checkups.id END) as qA'),
                DB::raw('COUNT(DISTINCT CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[1]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[1]['year'] . ' THEN elderly_checkups.id END) as qB'),
                DB::raw('COUNT(DISTINCT CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[2]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[2]['year'] . ' THEN elderly_checkups.id END) as qC'),
                DB::raw('COUNT(DISTINCT CASE WHEN QUARTER(elderly_checkups.checkupDate) = ' . $quartals[3]['quarter'] . ' AND YEAR(elderly_checkups.checkupDate) = ' . $quartals[3]['year'] . ' THEN elderly_checkups.id END) as qD'),
            )
            ->groupBy('pos.name')
            ->orderByRaw('LENGTH(pos.name), pos.name')
            ->get();
        $checkupData = [
            'labels' => [],
            'qA' => [],
            'qB' => [],
            'qC' => [],
            'qD' => [],
            'presentaseqA' => [],
            'presentaseqB' => [],
            'presentaseqC' => [],
            'presentaseqD' => [],
            'totalElderly' => [],
            'totalAll' => $checkup->sum('total_elderly')
        ];

        $totalQa = $checkup->sum('qA');
        $totalQb = $checkup->sum('qB');
        $totalQc = $checkup->sum('qC');
        $totalQd = $checkup->sum('qD');

        foreach ($checkup as $data) {
            $checkupData['labels'][] = $data->name;
            $checkupData['qA'][] = $data->qA;
            $checkupData['qB'][] = $data->qB;
            $checkupData['qC'][] = $data->qC;
            $checkupData['qD'][] = $data->qD;
            $checkupData['totalElderly'][] = $data->total_elderly;

            if ($totalQa > 0) {
                $checkupData['presentaseqA'][] = round(($data->qA / $data->total_elderly) * 100, 2);
            } else {
                $checkupData['presentaseqA'][] = 0;
            }

            if ($totalQb > 0) {
                $checkupData['presentaseqB'][] = round(($data->qB / $data->total_elderly) * 100, 2);
            } else {
                $checkupData['presentaseqB'][] = 0;
            }

            if ($totalQc > 0) {
                $checkupData['presentaseqC'][] = round(($data->qC / $data->total_elderly) * 100, 2);
            } else {
                $checkupData['presentaseqC'][] = 0;
            }

            if ($totalQd > 0) {
                $checkupData['presentaseqD'][] = round(($data->qD / $data->total_elderly) * 100, 2);
            } else {
                $checkupData['presentaseqD'][] = 0;
            }
        }

        // dd($checkupData);/

        $quartalOptions = $this->quartalOption();

        return view('home.attendanceGraph', compact('checkupData', 'totalQa', 'totalQb', 'totalQc', 'totalQd', 'quartals'));
    }


    private function getLastFourQuarters()
    {
        $currentQuarter = Carbon::now()->quarter;
        $currentYear = Carbon::now()->year;
        $quartalData = [];

        for ($i = 0; $i < 4; $i++) {
            $quartalData[] = [
                'quarter' => $currentQuarter,
                'year' => $currentYear,
            ];

            // Mengurangi satu kuartal dari data saat ini
            if ($currentQuarter == 1) {
                $currentQuarter = 4;
                $currentYear--;
            } else {
                $currentQuarter--;
            }
        }

        return array_reverse($quartalData);
    }

    private function getPenyakitOptions()
    {
        return [
            'gout' => 'Asam Urat',
            'cholesterol' => 'Kolesterol',
            'sugar' => 'Gula Darah',
            'tensionA' => 'Darah Tinggi',
        ];
    }




    public function quartalOption()
    {
        $currentYear = Carbon::now()->year;
        $currentQuarter = Carbon::now()->quarter; // Menghitung kuartal saat ini

        $options = [];

        $year = $currentYear;
        $quarter = $currentQuarter;

        for ($i = 0; $i < 4; $i++) {
            $label = 'Kuartal ' . $quarter . '-' . $year;
            $value = $quarter . '-' . $year;
            $options[$value] = $label;

            if ($quarter == 1) {
                $quarter = 4;
                $year--;
            } else {
                $quarter--;
            }
        }

        return $options;
    }





}
