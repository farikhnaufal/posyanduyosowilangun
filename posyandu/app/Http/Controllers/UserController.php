<?php

namespace App\Http\Controllers;

use App\Models\Pos;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $users = User::select('id', 'name', 'username', 'password', 'pos_id')
            ->orderByRaw("CAST(SUBSTRING_INDEX(pos_id, ' ', -1) AS UNSIGNED)")
            ->orderByRaw("LENGTH(pos_id), pos_id")
            ->get();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $pos = Pos::all();
        return view('users.create', compact('pos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required|min:5',
            'role' => 'required'

        ]);
        if ($validator->fails()) {
            # code...
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'pos_id' => $request->pos_id
        ]);

        if ($request->role == "admin") {
            # code...
            $user->assignRole('admin');
        } elseif ($request->role == "kader") {
            # code...
            $user->assignRole('kader');
        } elseif ($request->role == "koordinator") {
            # code...
            $user->assignRole('koordinator');
        } else {
            return redirect()->back();
        }

        return redirect('/users')->with('success', 'Pengguna berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $pos = Pos::all();
        $user = User::findOrFail($id);

        return view('users.edit', compact('user', 'pos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // Mengambil pengguna berdasarkan ID
        $user = User::findOrFail($id);


        // Validasi input
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users,username,' . $id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        // Update data pengguna
        $user->update([
            'name' => $request->name,
            'username' => $request->username,
            'pos_id' => $request->pos_id
        ]);

        if ($request->role == "admin") {
            # code...
            $user->syncRoles(['admin']);
        } elseif ($request->role == "kader") {
            # code...
            $user->syncRoles(['kader']);
        } elseif ($request->role == "koordinator") {
            # code...
            $user->syncRoles(['koordinator']);
        } else {
            return redirect()->back();
        }


        // Jika password diisi, update juga password
        if (!empty($request->password)) {
            // Validasi password baru
            $passwordValidator = Validator::make($request->all(), [
                'password' => 'required|min:5',
            ]);

            if ($passwordValidator->fails()) {
                return redirect()->back()->withInput()->withErrors($passwordValidator->errors());
            }

            // Update password dengan bcrypt
            $user->password = bcrypt($request->password);
            $user->save();
        }

        return redirect('/users')->with('success', 'Pengguna berhasil diubah');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back()->with('success', 'Data lansia berhasil dihapus');
    }
}
