<?php

namespace App\Http\Controllers;

use App\Models\Elderly;
use App\Models\ElderlyCheckup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ElderlyCheckupController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        // $elderlies = Elderly::with([
        //     'elderly_checkup' => function($q){$q->orderBy('checkupDate', 'DESC');}])->get();
        // return view('lansia.checkup.index', compact('elderlies'));
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        //
        $elderly = Elderly::findOrFail($request->elderly_id);
        return view('lansia.checkup.create', compact('elderly'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'weight' => 'required',
            'height' => 'required',
            'stomach' => 'required',
            'tensionA' => 'required',
            'tensionB' => 'required',
            'cholesterol' => 'required',
            'sugar' => 'required',
            'gout' => 'required',
            'independence' => 'required',
            'complaint',
            'diagnosis',
            'followUp' => 'required',
            'therapy',
            'checkupDate' => 'required',
            'elderly_id' => 'required',
        ]);
        if ($validator->fails()) {
            # code...
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        ElderlyCheckup::create([
            'weight' => $request->weight,
            'height' => $request->height,
            'stomach' => $request->stomach,
            'tensionA' => $request->tensionA,
            'tensionB' => $request->tensionB,
            'cholesterol' => $request->cholesterol,
            'sugar' => $request->sugar,
            'gout' => $request->gout,
            'independence' => $request->independence,
            'complaint' => $request->complaint,
            'diagnosis' => $request->diagnosis,
            'followUp' => $request->followUp,
            'therapy' => $request->therapy,
            'checkupDate' => $request->checkupDate,
            'elderly_id' => $request->elderly_id,
        ]);
        return redirect()->intended('/checkup/'.$request->elderly_id)->with('success', 'Data checkup berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $elderly = Elderly::findOrFail($id);
        $checkupQuarter = [];
        $checkupYear = [];

        foreach ($elderly->elderly_checkup as $checkup) {
            $checkupDate = Carbon::parse($checkup->checkupDate);
            $checkupQuarter[] = $checkupDate->quarter;
            $checkupYear[] = $checkupDate->year;
        }
        return view('lansia.checkup.show', compact('elderly', 'checkupQuarter', 'checkupYear'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(String $id)
    {
        //
        $checkup = ElderlyCheckup::findOrFail($id);
        return view('lansia.checkup.edit', compact('checkup'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'weight' => 'required',
            'height' => 'required',
            'stomach' => 'required',
            'tensionA' => 'required',
            'tensionB' => 'required',
            'cholesterol' => 'required',
            'sugar' => 'required',
            'gout' => 'required',
            'independence' => 'required',
            'complaint',
            'diagnosis',
            'followUp' => 'required',
            'therapy',
            'checkupDate' => 'required',
        ]);
        if ($validator->fails()) {
            # code...
            return redirect()->back()->withErrors($validator->errors());
        }

        $elderlyCheckup = ElderlyCheckup::findOrFail($id);

        $elderlyCheckup->update([
            'weight' => $request->weight,
            'height' => $request->height,
            'stomach' => $request->stomach,
            'tensionA' => $request->tensionA,
            'tensionB' => $request->tensionB,
            'cholesterol' => $request->cholesterol,
            'sugar' => $request->sugar,
            'gout' => $request->gout,
            'independence' => $request->independence,
            'complaint' => $request->complaint,
            'diagnosis' => $request->diagnosis,
            'followUp' => $request->followUp,
            'therapy' => $request->therapy,
            'checkupDate' => $request->checkupDate,
        ]);
        return redirect()->back()->with('success', 'Data checkup berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $checkup = ElderlyCheckup::findOrFail($id);
        $checkup->delete();

        return redirect()->back()->with('success', 'Data checkup berhasil dihapus');
    }
}
