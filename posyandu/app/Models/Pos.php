<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pos extends Model
{
    use HasFactory;
    protected $fillable =[
        'name',
    ];

    public function elderly(): HasMany{
        return $this->hasMany(Elderly::class);
    }
    public function user(): HasMany{
        return $this->hasMany(User::class);
    }
}
