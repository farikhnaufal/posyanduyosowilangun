<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Elderly extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'gender',
        'address',
        'bornPlace',
        'bornDate',
        'nik',
        'ktp',
        'disease',
        'family',
        'status',
        'familyPhone',
        'pos_id'
    ];

    public function pos(): BelongsTo{
        return $this->belongsTo(Pos::class);
    }

    public function elderly_checkup(): HasMany {
        return $this->hasMany(ElderlyCheckup::class);
    }
}
