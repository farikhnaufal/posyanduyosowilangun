<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ElderlyCheckup extends Model
{
    use HasFactory;
    protected $fillable = [
        'weight',
        'height',
        'stomach',
        'tensionA',
        'tensionB',
        'cholesterol',
        'sugar',
        'gout',
        'independence',
        'complaint',
        'diagnosis',
        'followUp',
        'therapy',
        'checkupDate',
        'elderly_id'

    ];
    public function elderly():BelongsTo {
        return $this->belongsTo(Elderly::class);
    }
}
