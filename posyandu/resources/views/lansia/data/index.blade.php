@extends('layouts')

@section('content')
<div class="bg-white p-4 rounded-3">
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <small>{{ session()->get('success') }}.</small>
            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="top-section d-flex justify-content-between">
        <h5 class="my-auto">
            Data Lansia
        </h5>

        @role(['admin','kader'])
        <a href="/lansia/create" class="btn btn-primary btn-sm">
            Tambah
        </a>
        @endrole
    </div>

    <div class="table-responsive my-4">
        <table id="example" class="table table-striped table-bordered" style="width: 100%;">
            <thead>
                <tr role="row" class="table-success">
                    <th class="d-none"></th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Pos</th>
                    <th>Alamat</th>
                    <th>Nama Wakil</th>
                    <th>No. Telp</th>
                    <th>Status</th>
                    <th style="width: 14em;">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($elderlies as $elderly)
                    <tr>
                        <td class="d-none"> {{ $elderly->created_at }}</td>
                        <td>{{ $elderly->nik }}</td>
                        <td>{{ $elderly->name }}</td>
                        <td>{{ $elderly->pos->name }}</td>
                        <td>{{ $elderly->address }}</td>
                        <td>{{ $elderly->family }}</td>
                        <td>{{ $elderly->familyPhone }}</td>
                        <td>{{ $elderly->status }}</td>
                        @role(['admin', 'kader'])
                        <td class="text-center">
                            <div class="row row-cols-2 row-cols-lg-4 g-3 g-lg-2">
                                <div class="col">
                                    <a href="/lansia/{{ $elderly->id }}" class="btn btn-small btn-secondary px-2 py-1"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Lihat">
                                        <i class='bi bi-eye text-white'></i>
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="/checkup/{{ $elderly->id }}" class="btn btn-small btn-primary px-2 py-1"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom"
                                        data-bs-title="Data Checkup">
                                        <i class='bi bi-heart-pulse text-white'></i>
                                    </a>
                                </div>
                                <div class="col">
                                    <a href="/lansia/{{ $elderly->id }}/edit"
                                        class="btn btn-small btn-warning px-2 py-1" data-bs-toggle="tooltip"
                                        data-bs-placement="bottom" data-bs-title="Ubah Lansia">
                                        <i class='bi bi-pencil text-white'></i>
                                    </a>
                                </div>
                                <div class="col">
                                    <form action="/lansia/{{ $elderly->id }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-small btn-danger px-2 py-1"
                                            data-bs-toggle="tooltip" data-bs-placement="bottom"
                                            data-bs-title="Hapus Lansia">
                                            <i class='bi bi-trash text-white'></i>

                                        </button>
                                    </form>
                                </div>
                            </div>
                        </td>
                        @elserole('koordinator')
                        <td class="text-center">
                            <div class="row row-cols-2 row-cols-lg-4 g-3 g-lg-2">

                                <div class="col">
                                    <a href="/checkup/{{ $elderly->id }}" class="btn btn-small btn-primary px-2 py-1"
                                        data-bs-toggle="tooltip" data-bs-placement="bottom"
                                        data-bs-title="Data Checkup">
                                        <i class='bi bi-heart-pulse text-white'></i>
                                    </a>
                                </div>

                            </div>
                        </td>
                        @endrole
                    </tr>
                @endforeach

            </tbody>

        </table>
    </div>
</div>

@stop
