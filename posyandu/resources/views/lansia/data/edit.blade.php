@extends('layouts')

@section('content')

<div class="bg-white p-4 p-lg-4 rounded-2">
    @if($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="z-index: 0;">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
        <small>{{session()->get('success')}}.</small>
        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="top-section d-flex justify-content-between">
        <h5 class="my-auto">
            Ubah Data Lansia
        </h5>

        <a href="/lansia" class="btn btn-sm btn-secondary">Kembali </a>
    </div>

    <form action="/lansia/{{$elderly->id}}" method="post" enctype="multipart/form-data" class="mt-4">
        @csrf
        @method('put')

        <div class="row row-cols-1 row-cols-lg-2">
            <div class="col">
                <div class="mb-2">
                    <label for="nik" class="form-label" >NIK</label>
                    <input type="number" name="nik" class="form-control" id="nik" value="{{$elderly->nik}}">
                </div>
            </div>

            <div class="col">
                <div class="mb-2">
                    <label for="name" class="form-label">Nama Lansia</label>
                    <input type="text" name="name" class="form-control" id="name" value="{{$elderly->name}}">
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="kependudukan" class="form-label">Kependudukan</label>
                    <select class="form-select" id="kependudukan" name="ktp" aria-label="Default select example">
                        <option value="1" @selected($elderly->ktp === "KTP Yosowilangun")>KTP Yosowilangun</option>
                        <option value="2" @selected($elderly->ktp === "Non KTP Yosowilangun")>Non KTP Yosowilangun</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="gender" class="form-label">Jenis Kelamin</label>
                    <select class="form-select" id="gender" name="gender" aria-label="Default select example">
                        <option value="1" @selected($elderly->gender === "Laki-Laki")>Laki-Laki</option>
                        <option value="2" @selected($elderly->gender === "Perempuan")>Perempuan</option>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="bornPlace" class="form-label">Tempat Lahir</label>
                    <input type="text" name="bornPlace" class="form-control" id="bornPlace" value="{{$elderly->bornPlace}}">
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="bornDate" class="form-label">Tanggal Lahir</label>
                    <input type="date" name="bornDate" class="form-control" id="bornDate" value="{{$elderly->bornDate}}">
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="pos" class="form-label">Pos</label>
                    @role('admin')
                    <select class="form-select" id="pos" name="pos_id" aria-label="Default select example">
                        <option value="">-- Pilih -</option>
                        @foreach($pos as $pos)
                            <option value="{{ $pos->id }}" @selected(old('pos_id') == $pos->id)>{{ $pos->name }}</option>
                        @endforeach
                    </select>
                    @elserole('kader')
                    <select class="form-select" id="pos" name="pos_id" aria-label="Default select example">
                        <option value="{{ Auth::user()->pos_id }}" selected>{{ Auth::user()->pos->name }}</option>
                    </select>
                    @endrole

                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="disease" class="form-label">Penyakit Bawaan</label>
                    <input type="text" name="disease" class="form-control" id="disease" value="{{$elderly->disease}}">
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="family" class="form-label">Keluarga yang dihubungi</label>
                    <input type="text" name="family" class="form-control" id="family" value="{{$elderly->family}}">
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="familyPhone" class="form-label">No. Telp Keluarga</label>
                    <input type="text" name="familyPhone" class="form-control" id="familyPhone" value="{{$elderly->familyPhone}}">
                </div>
            </div>
            <div class="col">
                <div class="form-outline mb-2">
                    <label class="form-label" for="address">Alamat Lengkap</label>
                    <textarea class="form-control" id="address" rows="4" name="address">{{$elderly->address}}</textarea>
                </div>
            </div>

            <div class="col">
                <div class="mb2">
                    <label for="status" class="form-label">Keterangan</label>
                    <select class="form-select" id="status" name="status" aria-label="Default select example">
                        <option value="1" @selected($elderly->status === "Hidup")>Hidup</option>
                        <option value="2" @selected($elderly->status === "Meninggal")>Meninggal</option>
                    </select>
                </div>
            </div>

        </div>


        <button class="btn btn-primary btn-lg mt-3 " type="submit">
            Ubah
        </button>

    </form>

</div>


@stop
