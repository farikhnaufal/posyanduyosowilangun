@extends('layouts')

@section('content')

<div class="bg-white p-4 rounded-3">
    <div class="ms-0">
        <a href="/lansia" class="btn btn-sm btn-secondary">Kembali </a>
    </div>

    <div class="table-responsive my-2">
        <table id="" class="table table-bordered" style="width: 100%;">
            <thead>
                <tr class="table-success">
                    <th colspan="3">DATA DIRI</th>
                </tr>
                <tr class="d-none">
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                
                <tr>
                    <td style="width: 20% !important;">NIK</td>
                    <td style="width: 5px !important;">:</td>
                    <td>{{ $elderly->nik }}</td>
                </tr>
                <tr>
                    <td>Nama Lengkap</td>
                    <td class="px-0 text-center">:</td>
                    <td>{{ $elderly->name }}</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td class="px-0 text-center">:</td>
                    <td>{{ $elderly->gender }}</td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td class="px-0 text-center">:</td>
                    <td>{{ $elderly->status }}</td>
                </tr>

            </tbody>
            <tr class="table-success">
                <th colspan="3">ALAMAT</th>
            </tr>
            <tr class="d-none">
                <th></th>
                <th></th>
                <th></th>
            </tr>

            <tr>
                <td>Kependudukan</td>
                <td class="px-0 text-center">:</td>
                <td>{{ $elderly->ktp }}</td>
            </tr>
            <tr>
                <td>Alamat Lengkap</td>
                <td class="px-0 text-center">:</td>
                <td>{{ $elderly->address }}</td>
            </tr>

            <tr class="table-success">
                <th colspan="3">DATA KELAHIRAN</th>
            </tr>
            <tr class="d-none">
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td>Tempat Lahir</td>
                <td class="px-0 text-center">:</td>
                <td>{{ $elderly->bornPlace }}</td>
            </tr>
            <tr>
                <td>Tanggal Lahir</td>
                <td class="px-0 text-center"> : </td>
                <td>{{ $elderly->bornDate }}</td>
            </tr>

            <tr class="table-success">
                <th colspan="3">DATA LAIN</th>
            </tr>
            <tr class="d-none">
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td>Penyakit Bawaan</td>
                <td class="px-0 text-center">:</td>
                <td>{{ $elderly->disease }}</td>
            </tr>
            <tr>
                <td>Pos Posyandu</td>
                <td class="px-0 text-center">:</td>
                <td>{{ $elderly->pos->name }}</td>
            </tr>

            <tr>
                <td>Keluarga</td>
                <td class="px-0 text-center">:</td>
                <td>{{ $elderly->family }}</td>
            </tr>
            <tr>
                <td>No.Telp Keluarga</td>
                <td class="px-0 text-center">:</td>
                <td>{{ $elderly->familyPhone }}</td>
            </tr>

        </table>
    </div>
</div>

@endsection
