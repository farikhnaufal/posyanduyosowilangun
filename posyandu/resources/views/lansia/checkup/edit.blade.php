@extends('layouts')

@section('content')

<div class="bg-white p-4 p-lg-4 rounded-2">
    @if($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="z-index: 0;">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <small>{{ session()->get('success') }}.</small>
            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif


    <div class="top-section d-flex justify-content-between">
        <h5 class="my-auto">
            Ubah Data Checkup
        </h5>

        <a href="/checkup/{{$checkup->elderly->id}}" class="btn btn-sm btn-secondary">Kembali </a>
    </div>

    <form action="/checkup/{{$checkup->id}}" method="post" enctype="multipart/form-data" class="mt-4">
        @csrf
        @method('put')
        <div class="row row-cols-2 row-cols-lg-4">
            <div class="col">
                <div class="mb-2">
                    <label for="checkupDate" class="form-label">Tgl Posyandu</label>
                    <input type="date" name="checkupDate" class="form-control" id="checkupDate" value="{{$checkup->checkupDate}}">
                </div>
            </div>
            

            <div class="col">
                <div class="mb-2">
                    <label for="weight" class="form-label">Berat Badan</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="weight" name="weight" step=0.01 value="{{$checkup->weight}}">
                        <span class="input-group-text" id="weight">Kg</span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="height" class="form-label">Tinggi Badan</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="height" name="height" step=0.01 value="{{$checkup->height}}">
                        <span class="input-group-text" id="height">Cm</span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="mb-2">
                    <label for="stomach" class="form-label">Lingkar Perut</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="stomach" name="stomach" step=0.01 value="{{$checkup->stomach}}">
                        <span class="input-group-text" id="stomach">Cm</span>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="mb-2">
                    <label for="tension" class="form-label">Tensi</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="tension" name="tensionA" step=0.001 value="{{$checkup->tensionA}}">
                        <span class="input-group-text" id="tension">/</span>
                        <input type="number" class="form-control" id="tension" name="tensionB" step=0.001 value="{{$checkup->tensionB}}">
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="mb-2">
                    <label for="cholesterol" class="form-label">Kolesterol</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="cholesterol" name="cholesterol" step=0.001 value="{{$checkup->cholesterol}}">
                        <span class="input-group-text" id="cholesterol">mg/dL</span>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="mb-2">
                    <label for="sugar" class="form-label">Gula</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="sugar" name="sugar" step=0.001 value="{{$checkup->sugar}}">
                        <span class="input-group-text" id="sugar">mg/dL</span>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="mb-2">
                    <label for="gout" class="form-label">Asam Urat</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="gout" name="gout" step=0.001 value="{{$checkup->gout}}">
                        <span class="input-group-text" id="gout">mg/dL</span>
                    </div>
                </div>
            </div>

            <div class="col">
                <label for="independece" class="form-label">Kemandirian</label>
                <select class="form-select" id="independence" name="independence" aria-label="Default select example">
                    <option value="1" @selected($checkup->independence === "Mandiri")>Mandiri</option>
                    <option value="2" @selected($checkup->independence === "Sebagian")>Sebagian</option>
                    <option value="3" @selected($checkup->independence === "Total")>Total</option>
                </select>
            </div>

            <div class="col">
                <label for="followUp" class="form-label">Tindak Lanjut</label>
                <select class="form-select" id="followUp" name="followUp" aria-label="Default select example">
                    <option value="1" @selected($checkup->followUp === "Rujukan")>Rujukan</option>
                    <option value="2" @selected($checkup->followUp === "Konseling")>Konseling</option>
                </select>
            </div>

            <div class="col">
                <div class="mb-2">
                    <label for="therapy" class="form-label">Terapi</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="therapy" name="therapy" value="{{$checkup->therapy}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-lg-2">
            <div class="col">
                <div class="form-outline mb-2">
                    <label class="form-label" for="complaint">Keluhan</label>
                    <textarea class="form-control" id="complaint" rows="4" name="complaint">{{$checkup->complaint}}</textarea>
                </div>
            </div>
            <div class="col">
                <div class="form-outline mb-2">
                    <label class="form-label" for="diagnosis">Diagnosa</label>
                    <textarea class="form-control" id="diagnosis" rows="4" name="diagnosis">{{$checkup->diagnosis}}</textarea>
                </div>
            </div>
        </div>


        <button class="btn btn-primary btn-lg mt-3 " type="submit">
            Ubah Data
        </button>

    </form>

</div>


@stop
