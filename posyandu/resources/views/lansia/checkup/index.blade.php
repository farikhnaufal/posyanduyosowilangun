@extends('layouts')

@section('content')

<div class="bg-white p-4 rounded-3">
    <div class="top-section d-flex justify-content-between">
        <h5 class="my-auto">
            Data Checkup Lansia
        </h5>

        <button class="btn btn-primary btn-small">
            Tambah
        </button>
    </div>

    <div class="table-responsive my-4">
        <table id="elderly-checkup-table" class="table table-striped table-bordered">
            <thead>
                <tr role="row" class="table-success">
                    <th>Checkup Terbaru</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Pos</th>
                    <th>Alamat</th>
                    <th>Checkup</th>
                    <th>Nama Wakil</th>
                    <th>No. Telp</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($elderlies as $elderly)
                    @if(!empty($elderly->elderly_checkup->first()->checkupDate))
                        <tr>
                            <td>
                                {{ $elderly->elderly_checkup->first()->checkupDate }}
                            </td>
                            <td>{{ $elderly->nik }}</td>
                            <td>{{ $elderly->name }}</td>
                            <td>{{ $elderly->pos->name }}</td>
                            <td>{{ $elderly->address }}</td>
                            <td>{{ $elderly->elderly_checkup->count() }} kali</td>
                            <td>{{ $elderly->family }}</td>
                            <td>{{ $elderly->familyPhone }}</td>
                            <td>
                                <a href="/checkup/{{ $elderly->id }}" class="btn btn-small btn-primary px-2 py-1"
                                    data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Lihat">
                                    <i class='bi bi-eye text-white'></i>
                                </a>

                            </td>
                        </tr>
                    @endif
                @endforeach

            </tbody>

        </table>
    </div>
</div>


@stop
