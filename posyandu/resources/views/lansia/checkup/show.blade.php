@extends('layouts')

@section('content')

<div class="bg-white p-4 rounded-3">

    @if($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="z-index: 0;">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <small>{{ session()->get('success') }}.</small>
            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="ms-0">
        <a href="/lansia" class="btn btn-sm btn-secondary">Kembali </a>
    </div>


    <div class="table-responsive my-3">
        <table class="table table-bordered" id="elderly-detail-table">
            <tbody>

                <tr>
                    <td style="width:15%">Nama Lansia</td>
                    <td style="width:1%">:</td>
                    <td id="elderlyName">{{ $elderly->name }}</td>
                </tr>
                <tr>
                    <td style="width:15%">Penanggung Jawab</td>
                    <td style="width:1%">:</td>
                    <td>{{ $elderly->family }}</td>
                </tr>
                <tr>
                    <td style="width:15%">Pos</td>
                    <td style="width:1%">:</td>
                    <td>{{ $elderly->pos->name }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="bg-white p-3 pt-lg-4 px-lg-4 pb-lg-1 rounded-3 mt-3">
    <div class="d-flex justify-content-between">
        <i class="bi bi-table"></i>
        <small class="ms-2 me-auto my-auto">Tabel Checkup Lansia</small>

        <form action="/checkup/create" method="get" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="elderly_id" value="{{ $elderly->id }}">
            <button class="btn btn-primary btn-sm">
                Tambah checkup
            </button>
        </form>
    </div>

    <div class="table-responsive my-3">
        <table class="table table-bordered table-striped " id="elderly-checkup-detail-table">

            <thead>
                <tr role="row" class="table-success">
                    <th class="d-hidden">Tanggal</th>
                    <th>Kuartal</th>
                    <th>Berat</th>
                    <th>Tinggi</th>
                    <th>Lingkar Perut</th>
                    <th>Tensi</th>
                    <th>Kolesterol</th>
                    <th>Gula</th>
                    <th>Asam Urat</th>
                    <th>Kemandirian</th>
                    <th>Keluhan</th>
                    <th>Diagnosa</th>
                    <th>Tindak Lanjut</th>
                    <th>Terapi</th>
                    <th style="width: 3rem;">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($elderly->elderly_checkup as $index => $checkup)
                    <tr>
                        <td class="d-hidden">{{ $checkup->checkupDate }}</td>
                        <td>{{$checkupQuarter[$index]}}({{$checkupYear[$index]}})</td>
                        <td>{{ $checkup->weight }} Kg</td>
                        <td>{{ $checkup->height }} Cm</td>
                        <td>{{ $checkup->stomach }} Cm</td>
                        <td>{{ $checkup->tensionA }}/{{ $checkup->tensionB }}</td>
                        <td>{{ $checkup->cholesterol }} mg/dL</td>
                        <td>{{ $checkup->sugar }} mg/dL</td>
                        <td>{{ $checkup->gout }} mg/dL</td>
                        <td>{{ $checkup->independence }}</td>
                        <td>{{ $checkup->complaint }}</td>
                        <td>{{ $checkup->diagnosis }}</td>
                        <td>{{ $checkup->followUp }}</td>
                        <td>{{ $checkup->therapy }}</td>
                        <td class="text-center">
                            <div class="row row-cols-2 g-3 g-lg-1">
                                <div class="col">
                                    <a href="/checkup/{{ $checkup->id }}/edit"
                                        class="btn btn-small btn-warning px-2 py-1" data-bs-toggle="tooltip"
                                        data-bs-placement="bottom" data-bs-title="Ubah Data">
                                        <i class='bi bi-pencil text-white'></i>
                                    </a>
                                </div>
                                <div class="col">
                                    <form action="/checkup/{{ $checkup->id }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-small btn-danger px-2 py-1"
                                            data-bs-toggle="tooltip" data-bs-placement="bottom"
                                            data-bs-title="Hapus Data">
                                            <i class='bi bi-trash text-white'></i>

                                        </button>
                                    </form>
                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

{{-- <div class="bg-white p-3 pt-lg-4 px-lg-4 pb-lg-1 rounded-3 mt-3">
    <div class="d-flex gap-2">
        <i class="bi bi-graph-down-arrow"></i>
        <small class="my-auto">Graph Checkup Lansia</small>
    </div>


    <div class="bg-white" style="width: auto; height: 400px;">

    </div>
</div> --}}

@endsection
