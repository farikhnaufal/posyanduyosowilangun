@extends('layouts')

@section('content')

<div class="row mx-3">
    <div class="col-lg-6 m-auto shadow-sm bg-white rounded-3 ">
        <div class="p-4">
            <div class="top-section d-flex justify-content-between mb-3">
                <h5 class="my-auto">
                    Ubah Pengguna
                </h5>

                <a href="/users" class="btn btn-sm btn-secondary">Kembali </a>
            </div>
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert" style="z-index: 0;">
                @foreach($errors->all() as $error)
                <small>{{ $error }}</small><br>
                @endforeach
                <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            @endif

            <form action="/users/{{$user->id}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="mb-2">
                    <label for="name" class="form-label">Nama</label>
                    <input type="text" name="name" class="form-control" id="name" value="{{$user->name}}">
                </div>
                <div class="mb-2">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" name="username" class="form-control" id="username" value="{{$user->username}}">
                </div>
                <div class="mb-2">
                    <label for="password" class="form-label">Kata sandi (Kosongi jika tidak ingin merubah kata sandi.)</label>
                    <input type="text" name="password" class="form-control" id="password">
                </div>
                <div class="mb-2">
                    <label for="role" class="form-label">Role</label>
                    <select class="form-select" id="role" name="role" aria-label="Default select example">
                        <option value="">-- Pilih -</option>
                        <option value="admin" @selected($user->roles->pluck('name')[0]=='admin')>Admin</option>
                        <option value="kader" @selected($user->roles->pluck('name')[0]=='kader')>Kader</option>
                        <option value="koordinator" @selected($user->roles->pluck('name')[0]=='koordinator')>Koordinator</option>

                    </select>
                </div>

                <div class="mb-2">
                    <label for="pos" class="form-label">Pos</label>
                    <select class="form-select" id="pos" name="pos_id" aria-label="Default select example">
                        <option value="">-- Pilih -</option>
                        @foreach($pos as $pos)
                        <option value="{{ $pos->id }}" @selected($user->pos_id == $pos->id)>{{ $pos->name }}</option>
                        @endforeach
                    </select>
                </div>

                <button class="btn btn-primary btn-lg mt-3 d-flex ms-auto me-0" type="submit">
                    Masuk
                </button>
            </form>
        </div>
    </div>
</div>


@stop
