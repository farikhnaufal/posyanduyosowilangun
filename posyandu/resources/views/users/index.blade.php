@extends('layouts')

@section('content')

<div class="row mx-3">
    <div class="col-lg-8 m-auto shadow-sm bg-white rounded-3 ">
        <div class="p-3">
            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <small>{{ session()->get('success') }}.</small>
                <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            <div class="top-section d-flex justify-content-between">
                <h5 class="my-auto">
                    Data Pengguna
                </h5>

                <a href="/users/create" class="btn btn-primary btn-sm">
                    Tambah
                </a>
            </div>

            <div class="table-responsive my-4">
                <table id="user-table" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                        <tr role="row" class="table-success">
                            <th class="d-none"></th>
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Role</th>
                            <th>Pos</th>
                            <th style="width: 7rem;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->name}}</td>
                            <td>{{ $user->getRoleNames() }}</td>
                            <td>{{ $user->pos->name ?? '-'}}</td>
                            <td class="text-center">
                                <div class="row row-cols-2 g-4 g-lg-2">
                                    <div class="col">
                                        <a href="/users/{{ $user->id }}/edit" class="btn btn-small btn-warning px-2 py-1" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Ubah User">
                                            <i class='bi bi-pencil text-white'></i>
                                        </a>
                                    </div>
                                    <div class="col">
                                        <form action="/users/{{ $user->id }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-small btn-danger px-2 py-1" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="Hapus User">
                                                <i class='bi bi-trash text-white'></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>


@stop