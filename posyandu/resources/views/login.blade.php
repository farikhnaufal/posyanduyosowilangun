@extends('layouts')


@section('content')
<div class="row mx-3 mt-5">
    <div class="col-lg-5 m-auto shadow-sm bg-white rounded-3 ">
        <div class="p-3 p-lg-4">
            <form action="/login" method="post" enctype="multipart/form-data">
                @csrf
                <h5 class="fw-bold text-dark-75 mb-2">
                    Masuk
                </h5>
                @if($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="z-index: 0;">
                    @foreach($errors->all() as $error)
                    <small>{{ $error }}</small>
                    @endforeach
                    <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif

                <div class="mb-2">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" name="username" class="form-control" id="username" value="{{old('username')}}">
                </div>

                <div class="mb-2">
                    <label for="password" class="form-label">Kata sandi</label>
                    <div class="input-group" id="show_hide_password">
                        <input type="password" name="password" class="form-control" id="password">

                        <span class="input-group-text">
                            <i class="bi bi-eye-slash" id="togglePassword" style="cursor: pointer"></i>
                        </span>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg mt-3 d-flex ms-auto me-0" type="submit">
                    Masuk
                </button>
            </form>
        </div>
    </div>
</div>

<script>
    const togglePassword = document.querySelector("#togglePassword");
    const password = document.querySelector("#password");

    togglePassword.addEventListener("click", function() {

        // toggle the type attribute
        const type = password.getAttribute("type") === "password" ? "text" : "password";
        password.setAttribute("type", type);
        // toggle the eye icon
        this.classList.toggle('bi-eye');
        this.classList.toggle('bi-eye-slash');
    });
</script>
@stop