<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Posyandu</title>
    <!-- JS TABLE -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <link href="https://cdn.datatables.net/v/bs5/dt-1.13.4/b-2.3.6/b-html5-2.3.6/kt-2.9.0/sb-1.4.2/datatables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/v/bs5/dt-1.13.4/b-2.3.6/b-html5-2.3.6/kt-2.9.0/sb-1.4.2/datatables.min.js">
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js" integrity="sha512-a9NgEEK7tsCvABL7KqtUTQjl69z7091EVPpw5KxPlZ93T141ffe1woLtbXTX+r2/8TtTvRX/v4zTL2UlMUPgwg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

  	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">

    <!-- CHART JS -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
    <!--  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400&display=swap" rel="stylesheet">


    <!-- Datatables -->
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css"> -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>

<body>
    <div class="sidebar d-none d-lg-block position-absolute left-0 shadow bg-lightdark text-light"
        style="overflow-y:auto; overflow-x: hidden; ">
        <div class="top-sidebar bg-primary">
            <h5 class="my-auto text-center fw-semibold text-white">POSYANDU</h5>
        </div>
        <ul class="list-unstyled">
          	<a class="text-decoration-none" href="https://desayosowilangun.gresikkab.go.id/">
				<div class="row align-items-center px-3 py-4">
                <div class="col-3">
                    <img src="{{ asset('images/logo.png') }}" alt="" srcset="" width="55px">
                </div>
                <div class="col-9 flex">
                    <p class="lh-sm pt-0 my-auto text-white">
                        Desa Yosowilangun Kecamatan Manyar Kabupaten Gresik
                    </p>
                </div>
            </div>
        	</a>

            @if(Auth::check())
            <div class="px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    MENU
                </small>
            </div>
            <a href="/lansia" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-folder2 fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Lansia
                    </small>
                </li>
            </a>

            <div class="mt-4 px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    Graph
                </small>
            </div>
            <a href="/" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-person fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Usia
                    </small>
                </li>
            </a>

            <a href="/kependudukan" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-postcard fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Kependudukan
                    </small>
                </li>
            </a>

            <a href="/quartal" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-calendar2-check fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Kegiatan)
                    </small>
                </li>
            </a>

            <a href="/penyakit" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-clipboard2-heart fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Penyakit)
                    </small>
                </li>
            </a>

            <a href="/kehadiran" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-ui-checks fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Kehadiran
                    </small>
                </li>
            </a>

            <div class="mt-4 px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    PENGATURAN
                </small>
            </div>
            @role('admin')
            <a href="/users" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-person fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Pengguna
                    </small>
                </li>
            </a>
            @endrole

            <li class="p-3">
                <a href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="d-flex gap-3 text-decoration-none">
                    <i class='bi bi-box-arrow-in-left fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Keluar akun
                    </small>
                </a>
                <form id="logout-form" action="/logout" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

            @else
            <div class="px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    Graph
                </small>
            </div>

            <a href="/" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-person fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Usia
                    </small>
                </li>
            </a>

            <a href="/kependudukan" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-postcard fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Kependudukan
                    </small>
                </li>
            </a>
            <a href="/quartal" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-calendar2-check fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Kegiatan)
                    </small>
                </li>
            </a>

            <a href="/penyakit" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-clipboard2-heart fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Penyakit)
                    </small>
                </li>
            </a>

            <a href="/kehadiran" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-ui-checks fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Kehadiran
                    </small>
                </li>
            </a>
            @endif
        </ul>
    </div>


    <nav class="navbar bg-primary text-white shadow-sm px-0 px-md-3 px-md-3">
        <div class="container-fluid d-flex px-2 py-2 py-lg-0">
            <h6 class="my-auto fw-semibold text-white d-lg-none mx-2">POSYANDU</h6>

            @if(Auth::check())
            <a class="hovered d-none d-lg-flex text-decoration-none justify-content-end ms-auto py-2 px-3 gap-2 rounded-3" data-bs-toggle="offcanvas" href="#lg-offcanvas" role="button" aria-controls="lg-offcanvas">
                <small class="text-white my-auto">
                    Hi,
                </small>

                <small class="text-white my-auto">
                    {{Auth::user()->name}}
                </small>
                <i class='bi bi-person fs-5 text-white'></i>
            </a>
            <a class="d-flex d-lg-none text-decoration-none justify-content-end ms-auto px-3 gap-2 rounded-3" data-bs-toggle="offcanvas" href="#sm-offcanvas" role="button" aria-controls="sm-offcanvas">
                <i class='bi bi-list fs-5 text-white'></i>
            </a>
            @else

            <div class="d-flex ms-auto me-0">
                <a href="/login" class="hovered d-flex text-decoration-none ms-auto me-0 py-2 px-3 gap-2 rounded-3">
                    <small class="text-white my-auto">
                        Masuk
                    </small>
                    <i class='bi bi-box-arrow-in-right d-none d-lg-block fs-5 text-white'></i>
                </a>
                <a class="d-flex d-lg-none text-decoration-none ms-auto px-3 gap-2 rounded-3" data-bs-toggle="offcanvas" href="#sm-offcanvas" role="button" aria-controls="sm-offcanvas">
                    <i class='bi bi-list fs-5 text-white'></i>
                </a>
            </div>

            @endif
        </div>
        <!-- <div class="container-fluid bg-white">
            <h6 id="clock" class="ms-auto me-2 lg-me-4 fw-semibold text-black-50 text-end my-auto"
                style="font-size: .8rem;">
            </h6>
        </div> -->
    </nav>

    <div class="content" style="z-index: -1;">
        @yield('content')
    </div>

    <!-- SMALL OFFCANVAS -->
    <div class="offcanvas offcanvas-start bg-lightdark text-semiwhite" tabindex="-1" id="sm-offcanvas" aria-labelledby="offcanvasExampleLabe" style="width: 18rem;">
        <div class="offcanvas-body p-0" style="overflow-x: hidden; overflow-y: auto">
          	<a class="text-decoration-none" href="https://desayosowilangun.gresikkab.go.id/">
				<div class="row align-items-center px-3 py-4">
                <div class="col-3">
                    <img src="{{ asset('images/logo.png') }}" alt="" srcset="" width="55px">
                </div>
                <div class="col-9 flex">
                    <p class="lh-sm pt-0 my-auto text-white">
                        Desa Yosowilangun Kecamatan Manyar Kabupaten Gresik
                    </p>
                </div>
            </div>
        	</a>

            <ul class="list-unstyled d-flex-col">
            @if(Auth::check())

            <div class="px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    MENU
                </small>
            </div>
            <a href="/lansia" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-folder2 fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Lansia
                    </small>
                </li>
            </a>

            <div class="mt-4 px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    Graph
                </small>
            </div>
            <a href="/" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-person fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Usia
                    </small>
                </li>
            </a>

            <a href="/kependudukan" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-postcard fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Kependudukan
                    </small>
                </li>
            </a>
            <a href="/quartal" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-postcard fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Kegiatan)
                    </small>
                </li>
            </a>
            <a href="/penyakit" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-clipboard2-heart fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Penyakit)
                    </small>
                </li>
            </a>
            <a href="/kehadiran" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-ui-checks fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Kehadiran
                    </small>
                </li>
            </a>

            <div class="mt-4 px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    PENGATURAN
                </small>
            </div>
            @role('admin')
            <a href="/users" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-person fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Pengguna
                    </small>
                </li>
            </a>
            @endrole

            <li class="p-3">
                <a href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="d-flex gap-3 text-decoration-none">
                    <i class='bi bi-box-arrow-in-left fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Keluar akun
                    </small>
                </a>
                <form id="logout-form" action="/logout" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

            @else
            <div class="px-3 py-2 bg-dark">
                <small class="text-semiwhite">
                    Graph
                </small>
            </div>

            <a href="/" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-person fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Usia
                    </small>
                </li>
            </a>

            <a href="/kependudukan" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-buildings fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Kependudukan
                    </small>
                </li>
            </a>
            <a href="/quartal" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-postcard fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Kegiatan)
                    </small>
                </li>
            </a>
            <a href="/penyakit" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-clipboard2-heart fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Checkup (Penyakit)
                    </small>
                </li>
            </a>
            <a href="/kehadiran" class="text-decoration-none">
                <li class="p-3 d-flex gap-3">
                    <i class='bi bi-ui-checks fs-6 my-auto text-semiwhite'></i>
                    <small class="my-auto text-semiwhite">
                        Data Kehadiran
                    </small>
                </li>
            </a>
            @endif
        </ul>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js" integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS" crossorigin="anonymous">
    </script>

    <!-- Data Tables -->
    <script src="{{ asset('js/script.js') }}"></script>

</body>


</html>
