@extends('layouts')
@section('content')


    <div class="row g-3">
        <div class="col-12">
            <div class="bg-white p-3 p-lg-4 rounded-3">
                <div class="d-flex gap-2">
                    <i class="bi bi-bar-chart"></i>
                    <small class="my-auto">Bar Chart Usia berdasarkan Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="age-bar-chart" height="350"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 justif-items-center">
            <div class="bg-white p-4 rounded-3 ">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase Lansia</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="age-pie-chart-lansia" height="350"></canvas>
                </div>
            </div>

            <div class="bg-white p-3 p-lg-4 rounded-3 mt-3">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase Pra Lansia</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="age-pie-chart-praLansia" height="350"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8">
            <div class="bg-white p-3 p-lg-4 rounded-3" style="height: max-content !important;">
                <div class="d-flex gap-2">
                    <i class="bi bi-table"></i>
                    <small class="my-auto">Tabel Usia berdasarkan Pos</small>
                </div>

                <div class="table-responsive mt-4">
                    <table class="table-bordered " style="width: 100%" style="border: black">

                        <thead class="bg-secondary-subtle" style="color: black; border:black;">
                            <tr role="row">
                                <th class="text-center" style="padding: 5px;" rowspan="2">POS</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Lansia</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Pra Lansia</th>
                                <th style="padding: 5px;" rowspan="2" class="text-center">Total</th>
                            </tr>
                            <tr role="row">
                                <th style="padding: 5px;" class="text-center">Jumlah (Orang)</th>
                                <th style="padding: 5px;" class="text-center">%</th>
                                <th style="padding: 5px;" class="text-center">Jumlah (Orang)</th>
                                <th style="padding: 5px;" class="text-center">%</th>
                            </tr>
                        </thead>
                        <tbody class="text-dark fw-bold" style="border: black">
                            @php
                                $colors = ['#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'];
                            @endphp

                            @foreach ($elderliesAgeData['labels'] as $index => $label)
                                <tr style="background-color: {{ $colors[$index] }} !important; height:2rem;">
                                    <td class="fw-bold text-center" style="padding: 5px;">{{ $label }}</td>
                                    <td class="text-center" style="padding: 5px;">{{ $elderliesAgeData['lansia'][$index] }} </td>
                                    <td style="padding: 5px;" class="text-center">{{ $elderliesAgeData['presentaseLansia'][$index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">{{ $elderliesAgeData['praLansia'][$index] }} </td>
                                    <td style="padding: 5px;" class="text-center">{{ $elderliesAgeData['presentasePraLansia'][$index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">
                                        {{ $elderliesAgeData['lansia'][$index] + $elderliesAgeData['praLansia'][$index] }}

                                        ({{ $elderliesAgeData['presentaseLansia'][$index] + $elderliesAgeData['presentasePraLansia'][$index] }}
                                        %)
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                        <tfoot class="bg-secondary-subtle">
                            <tr class="fw-bold" style= "background-color: white color: black;">
                                <td class="text-center" style="padding: 5px;">Jumlah</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalLansia }} </td>
                                <td class="text-center" style="padding: 5px;">{{ array_sum($elderliesAgeData['presentaseLansia']) }} %</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalPraLansia }} </td>
                                <td class="text-center" style="padding: 5px;">{{ array_sum($elderliesAgeData['presentasePraLansia']) }} %</td>
                                <td class="text-center" style="padding: 5px;" >
                                    {{ $totalLansia + $totalPraLansia }}
                                    {{-- ({{ round(($totalLansia / ($totalLansia + $totalPraLansia)) * 100, 2) + round(($totalPraLansia / ($totalLansia + $totalPraLansia)) * 100, 2) }} %) --}}
                                </td>
                            </tr>
                        </tfoot>



                    </table>
                </div>
            </div>
        </div>
    </div>







    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Mendapatkan data grafik dari blade template
            var elderliesAgeData = <?php echo json_encode($elderliesAgeData); ?>;
            const pastelColors = [
                '#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'
            ];

            // Mendapatkan elemen canvas
            var ctx = document.getElementById('age-bar-chart').getContext('2d');
            // Membuat objek grafik bar
            var ktpBarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: elderliesAgeData.labels,
                    datasets: [{
                            label: 'Pra Lansia',
                            data: elderliesAgeData.praLansia,
                            backgroundColor: 'rgba(54, 162, 235, 0.5)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Lansia',
                            data: elderliesAgeData.lansia,
                            backgroundColor: 'rgba(255, 99, 132, 0.5)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        x: {
                            display: true,
                            title: {
                                display: true,
                                text: 'POS'
                            }
                        },
                        y: {
                            display: true,
                            title: {
                                display: true,
                                text: 'Jumlah'
                            },
                            ticks: {
                                beginAtZero: true,
                                precision: 0
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            position: 'bottom',
                            align: 'start',
                        },
                    }
                },


            });

            var lansiaCanvas = document.getElementById('age-pie-chart-lansia').getContext('2d');

            // Menggambar grafik lansia dengan menggunakan Chart.js
            var lansiaChart = new Chart(lansiaCanvas, {
                type: 'pie',
                data: {
                    labels: elderliesAgeData.labels,
                    datasets: [{
                        label: 'KTP Yosowilangun',
                        data: elderliesAgeData.presentaseLansia,
                        backgroundColor: pastelColors.slice(0, elderliesAgeData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });

            // Membuat elemen canvas untuk grafik pra lansia
            var ktpNonYosoCanvas = document.getElementById('age-pie-chart-praLansia').getContext('2d');

            // Menggambar grafik pra lansia dengan menggunakan Chart.js
            var ktpNonYosoChart = new Chart(ktpNonYosoCanvas, {
                type: 'pie',
                data: {
                    labels: elderliesAgeData.labels,
                    datasets: [{
                        label: 'Non KTP Yosowilangun',
                        data: elderliesAgeData.presentasePraLansia,
                        backgroundColor: pastelColors.slice(0, elderliesAgeData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });
        });
    </script>


@stop
