@extends('layouts')
@section('content')

    <div class="bg-white p-3 p-lg-4 rounded-3">
        <div class="d-flex justify-content-between mb-4">
            <h6 class="my-auto d-none d-lg-flex ">
                Penyakit {{ $diseaseAlias }}
            </h6>

            <form action="/penyakit" method="get" class="d-flex ms-auto me-0 gap-2">
                <div class="d-block">
                    <select class="form-select form-select-sm" id="disease" name="disease"
                        aria-label="Default select example">
                        @foreach ($diseaseOptions as $value => $label)
                            <option value="{{ $value }}" @selected($disease == $value)>{{ $label }}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-primary  my-auto " type="submit">
                    Lihat
                </button>
            </form>
        </div>
        <div class="d-flex gap-2">
            <i class="bi bi-bar-chart"></i>
            <small class="my-auto">Bar Chart Penyakit {{$diseaseAlias}} per Kuartal berdasarkan Pos</small>
        </div>
        <div class="chart-wrapper mt-3" style="overflow-x: auto;">
            <div class="chart-container" style="width: 100em;">
                <canvas id="quartal-barchart" height="350"></canvas>
            </div>
        </div>
    </div>

    <div class="row g-3 mt-1">
        <div class="col-12 col-lg-4">
            <div class="bg-white p-4 rounded-3">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase {{$diseaseAlias}} <small class="fw-bold">{{$quartals[3]['year']}} (Q-{{$quartals[3]['quarter']}})</small> Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="qD-pie-chart" height="350"></canvas>
                </div>
            </div>

        </div>
        <div class="col-12 col-lg-4">

            <div class="bg-white p-4 rounded-3 ">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase {{$diseaseAlias}} <small class="fw-bold">{{$quartals[2]['year']}} (Q-{{$quartals[2]['quarter']}})</small> Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="qC-pie-chart" height="350"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="bg-white p-4 rounded-3">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase {{$diseaseAlias}} <small class="fw-bold">{{$quartals[1]['year']}} (Q-{{$quartals[1]['quarter']}})</small> Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="qB-pie-chart" height="350"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row g-3 mt-1">
        <div class="col-12 col-lg-4">
            <div class="bg-white p-4 rounded-3 ">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase {{$diseaseAlias}} <small class="fw-bold">{{$quartals[0]['year']}} (Q-{{$quartals[0]['quarter']}})</small> Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="qA-pie-chart" height="350"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8">
            <div class="bg-white p-3 p-lg-4 rounded-3" style="height: max-content !important;">
                <div class="d-flex gap-2">
                    <i class="bi bi-table"></i>
                    <small class="my-auto">Table Penyakit {{$disease}} per Kuartal berdasarkan Pos</small>
                </div>

                <div class="table-responsive mt-4">
                    <table class="table-bordered fw-bold" style="width: 100%">
                        <thead class="bg-secondary-subtle" style="border:black; color: black;">
                            <tr role="row">
                                <th class="text-center" style="padding: 5px;" rowspan="2">POS</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Kuartal {{$quartals[0]['quarter']}} {{($quartals[0]['year'])}}</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Kuartal {{$quartals[1]['quarter']}} {{($quartals[1]['year'])}}</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Kuartal {{$quartals[2]['quarter']}} {{($quartals[2]['year'])}}</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Kuartal {{$quartals[3]['quarter']}} {{($quartals[3]['year'])}}</th>
                                <th style="padding: 5px;" rowspan="2" class="text-center">Total</th>

                            </tr>
                            <tr role="row">
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                            </tr>
                        </thead>
                        <tbody class="text-dark fw-bold" style="border: black">
                            @php
                                $colors = ['#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'];
                            @endphp
                            @foreach ($checkupData['labels'] as $index => $label)
                                <tr style="background-color: {{ $colors[$index] }} !important; height:2rem;">

                                    <td style="padding: 5px;" class="fw-bold text-center">{{ $label }}</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['qA'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseqA'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['qB'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseqB'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['qC'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseqC'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['qD'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseqD'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">
                                        {{ $checkupData['qA'][$loop->index] + $checkupData['qB'][$loop->index] + $checkupData['qC'][$loop->index] + $checkupData['qD'][$loop->index] }}

                                        ({{ $checkupData['presentaseqA'][$loop->index] + $checkupData['presentaseqB'][$loop->index] + $checkupData['presentaseqC'][$loop->index] + $checkupData['presentaseqD'][$loop->index] }}
                                        %)
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot class="bg-secondary-subtle fw-bold" style="border: black">
                            <tr>
                                <td class="text-center" style="padding: 5px;">Jumlah</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalQa }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseqA']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalQb }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseqB']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalQc }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseqC']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalQd }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseqD']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ $totalQa + $totalQb + $totalQc + $totalQd }}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Mendapatkan data grafik dari blade template
            var checkupData = <?php echo json_encode($checkupData); ?>;
            var quartals = <?php echo json_encode($quartals); ?>;
            const pastelColors = [
                '#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'
            ];

            // Mendapatkan elemen canvas
            var ctx = document.getElementById('quartal-barchart').getContext('2d');
            // Membuat objek grafik bar
            var quartalBarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                            label: 'Q' + {{$quartals[0]['quarter']}} + '-' + {{$quartals[0]['year']}},
                            data: checkupData.qA,
                            backgroundColor: 'rgba(54, 162, 235, 0.5)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Q' + {{$quartals[1]['quarter']}} + '-' + {{$quartals[1]['year']}},
                            data: checkupData.qB,
                            backgroundColor: 'rgba(255, 99, 132, 0.5)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Q' + {{$quartals[2]['quarter']}} + '-' + {{$quartals[2]['year']}},
                            data: checkupData.qC,
                            backgroundColor: 'rgba(255, 205, 86, 0.5)',
                            borderColor: 'rgba(255, 205, 86, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Q' + {{$quartals[3]['quarter']}} + '-' + {{$quartals[3]['year']}},
                            data: checkupData.qD,
                            backgroundColor: 'rgba(75, 192, 192, 0.5)',
                            borderColor: 'rgba(75, 192, 192, 1)',
                            borderWidth: 1
                        }
                    ]
                },

                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        x: {
                            display: true,
                            title: {
                                display: true,
                                text: 'POS'
                            }
                        },
                        y: {
                            display: true,
                            title: {
                                display: true,
                                text: 'Jumlah'
                            },
                            ticks: {
                                beginAtZero: true,
                                precision: 0
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            position: 'bottom',
                            align: 'start',
                        },

                    },
                    // barThickness: 20,
                },



            });


            var qAcanvas = document.getElementById('qA-pie-chart').getContext('2d');
            var qAchart = new Chart(qAcanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: '-',
                        data: checkupData.presentaseqA,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });


            var qBcanvas = document.getElementById('qB-pie-chart').getContext('2d');
            var qBchart = new Chart(qBcanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: '-',
                        data: checkupData.presentaseqB,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });


            var qCcanvas = document.getElementById('qC-pie-chart').getContext('2d');
            var qCchart = new Chart(qCcanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: '-',
                        data: checkupData.presentaseqC,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });

            var qDcanvas = document.getElementById('qD-pie-chart').getContext('2d');
            var qDchart = new Chart(qDcanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: '-',
                        data: checkupData.presentaseqD,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });
        });
    </script>


@stop
