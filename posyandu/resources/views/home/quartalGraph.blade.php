@extends('layouts')
@section('content')

    <div class="bg-white p-3 p-lg-4 rounded-3">
        <div class="d-flex justify-content-between mb-4">
            <h6 class="my-auto d-none d-lg-flex">
                Kuartal {{ $quartal }}
            </h6>

            <form action="/quartal" method="get" class="d-flex ms-auto me-0 gap-2">
                <div class="d-block">
                    <select class="form-select form-select-sm" id="quartal" name="quartal"
                        aria-label="Default select example">
                        @foreach ($quartalOptions as $value => $label)
                            <option value="{{ $value }}" @selected($quartal == $value)>{{ $label }}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-primary  my-auto " type="submit">
                    Lihat
                </button>
            </form>
        </div>
        <div class="d-flex gap-2">
            <i class="bi bi-bar-chart"></i>
            <small class="my-auto">Bar Chart Penyakit Quartal berdasarkan Pos</small>
        </div>
        <div class="chart-wrapper mt-3" style="overflow-x: auto;">
            <div class="chart-container" style="width: 100em;">
                <canvas id="quartal-barchart" height="350"></canvas>
            </div>
        </div>
    </div>

    <div class="row g-3 mt-1">
        <div class="col-12 col-lg-4">
            <div class="bg-white p-4 rounded-3 ">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase Asam Urat Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="quartal-gout-pie-chart" height="350"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="bg-white p-4 rounded-3">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase Kolesterol Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="quartal-cholesterol-pie-chart" height="350"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="bg-white p-4 rounded-3 ">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase Gula Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="quartal-sugar-pie-chart" height="350"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row g-3 mt-1">
        <div class="col-12 col-lg-4">
            <div class="bg-white p-4 rounded-3">
                <div class="d-flex gap-2">
                    <i class="bi bi-pie-chart"></i>
                    <small class="my-auto">Presentase Darah Tinggi Tiap Pos</small>
                </div>
                <div class="chart-container mt-3">
                    <canvas id="quartal-tension-pie-chart" height="350"></canvas>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8">
            <div class="bg-white p-3 p-lg-4 rounded-3" style="height: max-content !important;">
                <div class="d-flex gap-2">
                    <i class="bi bi-table"></i>
                    <small class="my-auto">Tabel Penyakit berdasarkan Pos</small>
                </div>

                <div class="table-responsive mt-4">
                    <table class="table-bordered fw-bold" style="width: 100%">
                        <thead class="bg-secondary-subtle" style="border:black; color: black;">
                            <tr role="row">
                                <th class="text-center" style="padding: 5px;" rowspan="2">POS</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Asam Urat</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Kolesterol</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Gula</th>
                                <th style="padding: 5px;" colspan="2" class="text-center">Darah Tinggi</th>
                                <th style="padding: 5px;" rowspan="2" class="text-center">Total</th>

                            </tr>
                            <tr role="row">
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                                <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                                <th class="text-center" style="padding: 5px;">%</th>
                            </tr>
                        </thead>
                        <tbody class="text-dark fw-bold" style="border: black">
                            @php
                                $colors = ['#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'];
                            @endphp
                            @foreach ($checkupData['labels'] as $index => $label)
                                <tr style="background-color: {{ $colors[$index] }} !important; height:2rem;">

                                    <td style="padding: 5px;" class="fw-bold text-center">{{ $label }}</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['gout'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseGout'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['cholesterol'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseCholesterol'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['sugar'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseSugar'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['tension'][$loop->index] }} </td>
                                    <td class="text-center" style="padding: 5px;">{{ $checkupData['presentaseTension'][$loop->index] }} %</td>
                                    <td class="text-center" style="padding: 5px;">
                                        {{ $checkupData['gout'][$loop->index] + $checkupData['cholesterol'][$loop->index] + $checkupData['sugar'][$loop->index] + $checkupData['tension'][$loop->index] }}

                                        ({{ $checkupData['presentaseGout'][$loop->index] + $checkupData['presentaseCholesterol'][$loop->index] + $checkupData['presentaseSugar'][$loop->index] + $checkupData['presentaseTension'][$loop->index] }}
                                        %)
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot class="bg-secondary-subtle fw-bold" style="border: black">
                            <tr>
                                <td class="text-center" style="padding: 5px;">Jumlah</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalGout }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseGout']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalCholesterol }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseCholesterol']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalSugar }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseSugar']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">{{ $totalTension }} </td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ round(array_sum($checkupData['presentaseTension']), 2) }}
                                    %</td>
                                <td class="text-center" style="padding: 5px;">
                                    {{ $totalGout + $totalCholesterol + $totalSugar + $totalTension }}

                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Mendapatkan data grafik dari blade template
            var checkupData = <?php echo json_encode($checkupData); ?>;
            const pastelColors = [
                '#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'
            ];

            // Mendapatkan elemen canvas
            var ctx = document.getElementById('quartal-barchart').getContext('2d');
            // Membuat objek grafik bar
            var quartalBarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                            label: 'Asam Urat',
                            data: checkupData.gout,
                            backgroundColor: 'rgba(54, 162, 235, 0.5)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Kolesterol',
                            data: checkupData.cholesterol,
                            backgroundColor: 'rgba(255, 99, 132, 0.5)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Gula',
                            data: checkupData.sugar,
                            backgroundColor: 'rgba(255, 205, 86, 0.5)',
                            borderColor: 'rgba(255, 205, 86, 1)',
                            borderWidth: 1
                        },
                        {
                            label: 'Darah Tinggi',
                            data: checkupData.tension,
                            backgroundColor: 'rgba(75, 192, 192, 0.5)',
                            borderColor: 'rgba(75, 192, 192, 1)',
                            borderWidth: 1
                        }
                    ]
                },

                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        x: {
                            display: true,
                            title: {
                                display: true,
                                text: 'POS'
                            }
                        },
                        y: {
                            display: true,
                            title: {
                                display: true,
                                text: 'Jumlah'
                            },
                            ticks: {
                                beginAtZero: true,
                                precision: 0
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            position: 'bottom',
                            align: 'start',
                        },

                    },
                    // barThickness: 20,
                },



            });


            var goutCanvas = document.getElementById('quartal-gout-pie-chart').getContext('2d');
            var goutChart = new Chart(goutCanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: 'Asam Urat',
                        data: checkupData.presentaseGout,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });


            var cholesterolCanvas = document.getElementById('quartal-cholesterol-pie-chart').getContext('2d');
            var cholesterolChart = new Chart(cholesterolCanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: 'Asam Urat',
                        data: checkupData.presentaseCholesterol,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });


            var sugarCanvas = document.getElementById('quartal-sugar-pie-chart').getContext('2d');
            var sugarChart = new Chart(sugarCanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: 'Asam Urat',
                        data: checkupData.presentaseSugar,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });

            var tensionCanvas = document.getElementById('quartal-tension-pie-chart').getContext('2d');
            var tensionChart = new Chart(tensionCanvas, {
                type: 'pie',
                data: {
                    labels: checkupData.labels,
                    datasets: [{
                        label: 'Asam Urat',
                        data: checkupData.presentaseTension,
                        backgroundColor: pastelColors.slice(0, checkupData.labels.length),

                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    animation: {
                        animateRotate: true,
                        animateScale: true
                    },
                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.formattedValue + '%';
                                    return label;
                                }
                            }
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }
            });
        });
    </script>


@stop
