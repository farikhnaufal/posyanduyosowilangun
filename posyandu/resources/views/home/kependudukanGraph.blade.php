@extends('layouts')
@section('content')


<div class="row g-3">
    <div class="col-12">
        <div class="bg-white p-3 p-lg-4 rounded-3">
            <div class="d-flex gap-2">
                <i class="bi bi-bar-chart"></i>
                <small class="my-auto">Bar Chart Kependudukan berdasarkan Pos</small>
            </div>
            <div class="chart-container mt-3">
                <canvas id="kependudukan-bar-chart" height="350"></canvas>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4 justif-items-center">
        <div class="bg-white p-4 rounded-3 ">
            <div class="d-flex gap-2">
                <i class="bi bi-pie-chart"></i>
                <small class="my-auto">Presentase KTP Yosowilangun</small>
            </div>
            <div class="chart-container mt-3">
                <canvas id="kependudukan-pie-chart-ktpYoso" height="350"></canvas>
            </div>
        </div>

        <div class="bg-white p-3 p-lg-4 rounded-3 mt-3">
            <div class="d-flex gap-2">
                <i class="bi bi-pie-chart"></i>
                <small class="my-auto">Presentase Non KTP Yosowilangun</small>
            </div>
            <div class="chart-container mt-3">
                <canvas id="kependudukan-pie-chart-ktpNonYoso" height="350"></canvas>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-8">
        <div class="bg-white p-3 p-lg-4 rounded-3" style="height: max-content !important;">
            <div class="d-flex gap-2">
                <i class="bi bi-table"></i>
                <small class="my-auto">Tabel Kependudukan berdasarkan Pos</small>
            </div>

            <div class="table-responsive mt-4">
                <table class="table-bordered fw-bold" style="width: 100%">
                    <thead class="bg-secondary-subtle" style="color: black; border: black">
                        <tr role="row">
                            <th class="text-center" style="padding: 5px;" rowspan="2">POS</th>
                            <th style="padding: 5px;" colspan="2" class="text-center">KTP Yosowilangun</th>
                            <th style="padding: 5px;" colspan="2" class="text-center">Non KTP Yosowilangun</th>
                            <th style="padding: 5px;" rowspan="2" class="text-center">Total</th>
                        </tr>
                        <tr role="row">
                            <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                            <th class="text-center" style="padding: 5px;">%</th>
                            <th class="text-center" style="padding: 5px;">Jumlah (Orang)</th>
                            <th class="text-center" style="padding: 5px;">%</th>
                        </tr>
                    </thead>
                    <tbody class="text-black" style="border: black">
                        @php
                            $colors = ['#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'];
                        @endphp
                        @foreach($elderliesKtpData['labels'] as $index => $label)
                        <tr style="background-color: {{$colors[$index]}} !important; height:2rem;">

                            <td style="padding: 5px;" class="fw-bold text-center">{{$label}}</td>
                            <td class="text-center" style="padding: 5px;">{{$elderliesKtpData['ktpYoso'][$loop->index]}} </td>
                            <td class="text-center" style="padding: 5px;">{{$elderliesKtpData['presentaseKtpYoso'][$loop->index]}} %</td>
                            <td class="text-center" style="padding: 5px;">{{$elderliesKtpData['ktpNonYoso'][$loop->index]}} </td>
                            <td class="text-center" style="padding: 5px;">{{$elderliesKtpData['presentaseKtpNonYoso'][$loop->index]}} %</td>
                            <td class="text-center" style="padding: 5px;">
                                {{$elderliesKtpData['ktpYoso'][$loop->index] + $elderliesKtpData['ktpNonYoso'][$loop->index]}}
                                ({{$elderliesKtpData['presentaseKtpYoso'][$loop->index] + $elderliesKtpData['presentaseKtpNonYoso'][$loop->index]}} %)
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot class="bg-secondary-subtle fw-bold" style="border: black">
                        <tr>
                            <td class="text-center" style="padding: 5px;">Jumlah</td>
                            <td class="text-center" style="padding: 5px;">{{$totalKtpYoso}} </td>
                            <td class="text-center" style="padding: 5px;">{{array_sum($elderliesKtpData['presentaseKtpYoso'])}} %</td>
                            <td class="text-center" style="padding: 5px;">{{$totalKtpNonYoso}} </td>
                            <td class="text-center" style="padding: 5px;">{{array_sum($elderliesKtpData['presentaseKtpNonYoso'])}} %</td>
                            <td class="text-center" style="padding: 5px;">
                                {{$totalKtpYoso + $totalKtpNonYoso}}
                                {{-- ({{round($totalKtpYoso / ($totalKtpYoso + $totalKtpNonYoso) * 100, 2) +  round($totalKtpNonYoso / ($totalKtpYoso + $totalKtpNonYoso) * 100, 2)}} %) --}}
                            </td>
                        </tr>
                    </tfoot>



                </table>
            </div>
        </div>
    </div>
</div>







<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Mendapatkan data grafik dari blade template
        var elderliesKtpData = <?php echo json_encode($elderliesKtpData); ?>;
        const pastelColors = [
            '#5C8984', '#FFD966', '#6096B4', '#E97777', '#94D0CC', '#C6A9A3', '#CD5D7D', '#BBEAA6', '#FED9CA', '#9873B9', '#EF6C57', '#87A8D0', '#DFD3C3', '#EDA1C1', '#DDE8B9', '#D8AED3'
        ];

        // Mendapatkan elemen canvas
        var ctx = document.getElementById('kependudukan-bar-chart').getContext('2d');
        // Membuat objek grafik bar
        var ktpBarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: elderliesKtpData.labels,
                datasets: [{
                        label: 'Non KTP Yosowilangun',
                        data: elderliesKtpData.ktpNonYoso,
                        backgroundColor: 'rgba(54, 162, 235, 0.5)',
                        borderColor: 'rgba(54, 162, 235, 1)',
                        borderWidth: 1
                    },
                    {
                        label: 'KTP Yosowilangun',
                        data: elderliesKtpData.ktpYoso,
                        backgroundColor: 'rgba(255, 99, 132, 0.5)',
                        borderColor: 'rgba(255, 99, 132, 1)',
                        borderWidth: 1
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    x: {
                        display: true,
                        title: {
                            display: true,
                            text: 'POS'
                        }
                    },
                    y: {
                        display: true,
                        title: {
                            display: true,
                            text: 'Jumlah'
                        },
                        ticks: {
                            beginAtZero: true,
                            precision: 0
                        }
                    }
                },
                plugins: {
                    legend: {
                        position: 'bottom',
                        align: 'start',
                    },
                }
            },


        });

        var lansiaCanvas = document.getElementById('kependudukan-pie-chart-ktpYoso').getContext('2d');

        // Menggambar grafik ktpYoso dengan menggunakan Chart.js
        var lansiaChart = new Chart(lansiaCanvas, {
            type: 'pie',
            data: {
                labels: elderliesKtpData.labels,
                datasets: [{
                    label: 'KTP Yosowilangun',
                    data: elderliesKtpData.presentaseKtpYoso,
                    backgroundColor: pastelColors.slice(0, elderliesKtpData.labels.length),

                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                animation: {
                    animateRotate: true,
                    animateScale: true
                },
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                var label = context.label || '';
                                if (label) {
                                    label += ': ';
                                }
                                label += context.formattedValue + '%';
                                return label;
                            }
                        }
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }
        });

        // Membuat elemen canvas untuk grafik pra ktpYoso
        var ktpNonYosoCanvas = document.getElementById('kependudukan-pie-chart-ktpNonYoso').getContext('2d');

        // Menggambar grafik pra ktpYoso dengan menggunakan Chart.js
        var ktpNonYosoChart = new Chart(ktpNonYosoCanvas, {
            type: 'pie',
            data: {
                labels: elderliesKtpData.labels,
                datasets: [{
                    label: 'Non KTP Yosowilangun',
                    data: elderliesKtpData.presentaseKtpNonYoso,
                    backgroundColor: pastelColors.slice(0, elderliesKtpData.labels.length),

                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                animation: {
                    animateRotate: true,
                    animateScale: true
                },
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                var label = context.label || '';
                                if (label) {
                                    label += ': ';
                                }
                                label += context.formattedValue + '%';
                                return label;
                            }
                        }
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }
        });
    });
</script>


@stop
